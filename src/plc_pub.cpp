#include <ros/ros.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <sensor_msgs/PointCloud2.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseArray.h>
#include <pcl_conversions/pcl_conversions.h>
#include <iostream>
#include <math.h>
#include <armadillo>
using namespace std;
using namespace arma;

typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;
PointCloud::Ptr msg (new PointCloud);
geometry_msgs::Pose agent1, agent2, agent3, agent4;
geometry_msgs::PoseArray agent_pose;

void pose_callback1(const geometry_msgs::Pose::ConstPtr& msg_sub)
{
  agent1 = *msg_sub;
}

void pose_callback2(const geometry_msgs::Pose::ConstPtr& msg_sub)
{
  agent2 = *msg_sub;
}

void pose_callback3(const geometry_msgs::Pose::ConstPtr& msg_sub)
{
  agent3 = *msg_sub;
}

void pose_callback4(const geometry_msgs::Pose::ConstPtr& msg_sub)
{
  agent4 = *msg_sub;
}

mat AngletoR(mat ang, mat pos)
{
  mat T_arma;
  T_arma << cos(ang[0,0])*cos(ang[0,1]) << cos(ang[0,0])*sin(ang[0,1])*sin(ang[0,2])-sin(ang[0,0])*cos(ang[0,2]) << cos(ang[0,0])*sin(ang[0,1])*cos(ang[0,2])+sin(ang[0,0])*sin(ang[0,2]) << pos(0,0) << endr
	 << sin(ang[0,0])*cos(ang[0,1]) << sin(ang[0,0])*sin(ang[0,1])*sin(ang[0,2])+cos(ang[0,0])*cos(ang[0,2]) << sin(ang[0,0])*sin(ang[0,1])*cos(ang[0,2])-cos(ang[0,0])*sin(ang[0,2]) << pos(1,0) << endr
	 << -sin(ang[0,1]) << cos(ang[0,1])*sin(ang[0,2]) << cos(ang[0,1])*cos(ang[0,2]) << pos(2,0) << endr
   << 0 << 0 << 0 << 1 << endr;

  return T_arma;
}

void square_obs()
{
  float x_obs[4] = {6.4, 2.6, 6, 9.9};
  float y_obs[4] = {4.6, 8.5, 12.4, 8.1};
  unsigned char point_num = 10;
  float grad_x, grad_y;

  msg->width = msg->width + 4*point_num;
  // msg->width = 1;

  for(unsigned char i = 0; i < 4; i++)
  {

    if(i!=3)
    {
      grad_x = (x_obs[i+1] - x_obs[i])/ ((float) (point_num));
      grad_y = (y_obs[i+1] - y_obs[i])/ ((float) (point_num));
    }
    else
    {
      grad_x = (x_obs[0] - x_obs[i])/ ((float) (point_num));
      grad_y = (y_obs[0] - y_obs[i])/ ((float) (point_num));
    }

    for(unsigned char j = 0; j < point_num; j++)
    {
      msg->points.push_back (pcl::PointXYZ(x_obs[i]+grad_x*j, y_obs[i]+grad_y*j, 0.0));
    }
  }
}

void plane_obs(float x_cen, float y_cen, float z_cen, float plane_length, float alpha, float beta, float gamma)
{
  float x_obs[4];
  float y_obs[4];
  float z_obs[4];
  unsigned char corner=0;
  char signs_i, signs_j;
  mat obs_vec, obs_rot, obs_new;

  for(unsigned char i=0; i<2; i++)
  {
    for(unsigned char j=0; j<2; j++)
    {
      if(i%2==0) signs_i = -1;
      else signs_i = 1;

      if(j%2==0) signs_j = -1;
      else signs_j = 1;

      x_obs[corner] = plane_length/2*(signs_i);
      y_obs[corner] = 0;
      z_obs[corner] = plane_length/2*(signs_j);

      corner++;
    }
  }

  // unsigned char point_num = 10;
  unsigned char point_num = 20;
  float grad_x, grad_y, grad_z;
  msg->width = msg->width + pow(point_num,2);
  // msg->width = 1;
  grad_x = grad_z = plane_length/((float)(point_num));
  mat angles, positions;
  for(unsigned char i = 0; i < point_num; i++)
  {
    for(unsigned char j = 0; j < point_num; j++)
    {
      obs_vec << x_obs[0]+grad_x*j << endr
              << y_obs[0] << endr
              << z_obs[0]+grad_z*i << endr
              << 1 << endr;

      angles << alpha << endr
            << beta << endr
            << gamma << endr;
      positions << x_cen << endr
                << y_cen << endr
                << z_cen << endr;
      obs_rot = AngletoR(angles, positions);
      // obs_rot << cos(theta) << sin(theta) << 0 << x_cen << endr
      //         << -sin(theta) << cos(theta) << 0 << y_cen << endr
      //         << 0 << 0 << 1 << z_cen << endr
      //         << 0 << 0 << 0 << 1 << endr;
      obs_new = obs_rot*obs_vec;
      msg->points.push_back (pcl::PointXYZ(obs_new(0,0), obs_new(1,0), obs_new(2,0)));
    }
  }
}

void line_obs(float x_cen, float y_cen, float plane_length, float theta)
{
  float x_obs[2];
  float y_obs[2];

  unsigned char corner=0;
  char signs_i, signs_j;
  mat obs_vec, obs_rot, obs_new;

  for(unsigned char i=0; i<2; i++)
  {
    if(i%2==0) signs_i = -1;
    else signs_i = 1;

    x_obs[corner] = plane_length/2*(signs_i);
    y_obs[corner] = 0;

    corner++;
  }

  unsigned char point_num = 20;
  float grad_x;
  msg->width = msg->width + point_num;
  // msg->width = 1;
  grad_x = plane_length/((float)(point_num));
  mat angles, positions;

  for(unsigned char j = 0; j < point_num; j++)
  {
    obs_vec << x_obs[0]+grad_x*j << endr
            << y_obs[0] << endr
            << 0 << endr
            << 1 << endr;
    // obs_rot = AngletoR(angles, positions);
    obs_rot << cos(theta) << sin(theta) << 0 << x_cen << endr
            << -sin(theta) << cos(theta) << 0 << y_cen << endr
            << 0 << 0 << 1 << 0 << endr
            << 0 << 0 << 0 << 1 << endr;
    obs_new = obs_rot*obs_vec;

    msg->points.push_back (pcl::PointXYZ(obs_new(0,0), obs_new(1,0), obs_new(2,0)));
  }
}

void cube_obs(float x_cen, float y_cen, float z_cen, float plane_length, float theta)
{
  char signs_i, signs_j;
  float y_obs[2];
  for(unsigned char j=0; j<2; j++)
  {
    if(j%2==0) signs_j = -1;
    else signs_j = 1;

    y_obs[j] = y_cen+plane_length/2*(signs_j);
    plane_obs(x_cen, y_obs[j], z_cen, plane_length, theta, 0, 0);
    // plane_obs(x_cen+plane_length/2*(signs_j), y_obs[j]+plane_length/2, z_cen, plane_length, M_PI/2+theta, 0, 0);
  }

}

void circle_obs(float x_cen, float y_cen, float radius)
{
  unsigned char point_num = 40;

  msg->width = msg->width + point_num;
  // msg->width = 1;

  for(unsigned char i = 0; i < point_num; i++)
  {
    msg->points.push_back (pcl::PointXYZ(x_cen+radius*cos(2*M_PI/((float)(point_num))*i), y_cen+radius*sin(2*M_PI/((float)(point_num))*i), 0.0));
  }
}

void arc_circle_obs(float x_cen, float y_cen, float radius, float angle)
{
  unsigned char point_num = 40;

  msg->width = msg->width + point_num;
  // msg->width = 1;

  for(unsigned char i = 0; i < point_num; i++)
  {
    msg->points.push_back (pcl::PointXYZ(x_cen+radius*cos(angle/((float)(point_num))*(i-point_num/2)), y_cen+radius*sin(angle/((float)(point_num))*(i-point_num/2)), 0.0));
  }
}


void spherical_obs(float x_cen, float y_cen, float z_cen, float radius)
{
  unsigned int point_num = 20;

  msg->width = msg->width + pow(point_num,2);
  // msg->width = 1;

  for(unsigned int i = 0; i < point_num; i++)
  {
    for(unsigned int j = 0; j< point_num; j++)
    {
      msg->points.push_back (pcl::PointXYZ(x_cen+radius*sin(2*M_PI/((float)(point_num))*j)*cos(2*M_PI/((float)(point_num))*i), y_cen+radius*sin(2*M_PI/((float)(point_num))*j)*sin(2*M_PI/((float)(point_num))*i), z_cen+radius*cos(2*M_PI/((float)(point_num))*j)));
    }
  }
}

void double_spherical_obs()
{
  float x_cen = 6;
  float y_cen = 6;
  float z_cen = 0;
  float radius = 2;
  unsigned char point_num = 40;

  msg->width = msg->width + 2*pow(point_num,2);
  // msg->width = 1;

  for(unsigned char i = 0; i < point_num; i++)
  {
    for(unsigned char j = 0; j< point_num; j++)
    {
      msg->points.push_back (pcl::PointXYZ(x_cen+radius*sin(2*M_PI/((float)(point_num))*j)*cos(2*M_PI/((float)(point_num))*i), y_cen+radius*sin(2*M_PI/((float)(point_num))*j)*sin(2*M_PI/((float)(point_num))*i), z_cen+radius*cos(2*M_PI/((float)(point_num))*j)));
    }
  }

  x_cen = 6;
  y_cen = 6;
  z_cen = 2;
  radius = 2;

  for(unsigned char i = 0; i < point_num; i++)
  {
    for(unsigned char j = 0; j< point_num; j++)
    {
      msg->points.push_back (pcl::PointXYZ(x_cen+radius*sin(2*M_PI/((float)(point_num))*j)*cos(2*M_PI/((float)(point_num))*i), y_cen+radius*sin(2*M_PI/((float)(point_num))*j)*sin(2*M_PI/((float)(point_num))*i), z_cen+radius*cos(2*M_PI/((float)(point_num))*j)));
    }
  }

}

void cylinder(float x_cen, float y_cen, float z_cen, float radius, float height)
{
  unsigned int point_num = 20;

  msg->width = msg->width + pow(point_num,2);
  // msg->width = 1;
  int counter_print=0;
  for(unsigned char j = 0; j < point_num; j++)
  {
    for(unsigned char i = 0; i < point_num; i++)
    {
      msg->points.push_back (pcl::PointXYZ(x_cen+radius*cos(2*M_PI/((float)(point_num))*i), y_cen+radius*sin(2*M_PI/((float)(point_num))*i), z_cen - (height/2) + height*j/(float)(point_num)));
      counter_print++;
    }
  }
  // cout << counter_print << endl;
}

void forest_3d()
{
  unsigned char tree_num = 4;
  float scaling = 2.0;
  float tree_height, tree_rad;
  for(unsigned char i=0; i<tree_num; i++)
  {
    for(unsigned char j=0; j<tree_num; j++)
    {
      tree_height = (float)(rand() % 10)*0.1+6;
      tree_rad = (float)(rand() % 10)*0.1+1.5;
      cylinder(10+i*scaling,-tree_num*0.5+j*scaling,tree_height/2,0.3,tree_height);
      spherical_obs(10+i*scaling,-tree_num*0.5+j*scaling,tree_height+tree_rad,tree_rad);
    }
  }

  // Add floor to the forest
  // plane_obs(12, 0, 0, 14, 0, 0, M_PI/2);
  // plane_obs(12, 0, -14, 14, 0, 0, M_PI/2);
  // plane_obs(5, -2, -7, 14, M_PI/2, 0, 0);
  // plane_obs(19, -2, -7, 14,  M_PI/2, 0, 0);

  // for(unsigned char i=0; i<28; i++)
  // {
  //   plane_obs(12, 0, -i*0.5, 20, 0, 0, M_PI/2);
  // }

}

void forest_2d()
{
  unsigned char tree_num_y = 12;
  unsigned char tree_num_x = 6;
  float scaling = 1.5;
  float tree_rad;
  for(unsigned char i=0; i<tree_num_x; i++)
  {
    for(unsigned char j=0; j<tree_num_y; j++)
    {
      tree_rad = (float)(rand() % 2)*0.1+0.5;
      // circle_obs(10+i*scaling,-tree_num*0.5+j*scaling,tree_rad);
      circle_obs(10+i*scaling+5*cos(-M_PI/2+M_PI/((float)(tree_num_y))*j), 5*sin(-M_PI/2+M_PI/((float)(tree_num_y))*j),tree_rad);
    }
  }
}

void agent_pose_array()
{
  agent_pose.poses.clear();
  agent_pose.poses.push_back(agent1);
  agent_pose.poses.push_back(agent2);
  agent_pose.poses.push_back(agent3);
  agent_pose.poses.push_back(agent4);
}

void agent_obs(unsigned char agent_num)
{
  msg->width = msg->width + agent_num;
  for(unsigned char i=0; i<agent_num; i++)
  {
    msg->points.push_back(pcl::PointXYZ(agent_pose.poses[i].position.x, agent_pose.poses[i].position.y, agent_pose.poses[i].position.z));
  }

}

int main(int argc, char** argv)
{
  ros::init (argc, argv, "pub_pcl");
  ros::NodeHandle nh;
  ros::Publisher pub = nh.advertise<PointCloud> ("/obstacle_point", 10);
  ros::Subscriber sub1 = nh.subscribe<geometry_msgs::Pose>("/agent1/agent/pose", 10, pose_callback1);
  ros::Subscriber sub2 = nh.subscribe<geometry_msgs::Pose>("/agent2/agent/pose", 10, pose_callback2);
  ros::Subscriber sub3 = nh.subscribe<geometry_msgs::Pose>("/agent3/agent/pose", 10, pose_callback3);
  ros::Subscriber sub4 = nh.subscribe<geometry_msgs::Pose>("/agent4/agent/pose", 10, pose_callback4);

  msg->header.frame_id = "world";
  msg->height = 1;

  // square_obs();
  // circle_obs();
  // spherical_obs(100, 100, 5, 2);
  // double_spherical_obs();
  // plane_obs(5,5,2,4,-M_PI/4,0,0);
  // plane_obs(20,20,2,4,-M_PI/4,0,0);

  // U_Shaped RAL_IROS 2018
  // plane_obs(6,6,2,4,-M_PI/4,0,0);
  // plane_obs(6+2*cos(M_PI/4),6+2*cos(M_PI/4),4+2*cos(M_PI/4),4,-M_PI/4,0,-M_PI/4);
  // plane_obs(6+4*cos(M_PI/4)+1,6+4*cos(M_PI/4)+1,4+4*cos(M_PI/4),4,-M_PI/4,0,-M_PI/2);
  // plane_obs(6+4*cos(M_PI/4)+3+1,6+4*cos(M_PI/4)+3+1,4+4*cos(M_PI/4),4,-M_PI/4,0,-M_PI/2);
  // plane_obs(6+4*cos(M_PI/4)+3+3+1,6+4*cos(M_PI/4)+3+3+1,4+4*cos(M_PI/4),4,-M_PI/4,0,-M_PI/2);
  // plane_obs(6+2*cos(M_PI/4),6+2*cos(M_PI/4),4-2*cos(M_PI/4)-4,4,-M_PI/4,0,M_PI/4);
  // plane_obs(6+4*cos(M_PI/4)+1,6+4*cos(M_PI/4)+1,4-4*cos(M_PI/4)-4,4,-M_PI/4,0,-M_PI/2);
  // plane_obs(6+4*cos(M_PI/4)+3+1,6+4*cos(M_PI/4)+3+1,4-4*cos(M_PI/4)-4,4,-M_PI/4,0,-M_PI/2);
  // plane_obs(6+4*cos(M_PI/4)+3+3+1,6+4*cos(M_PI/4)+3+3+1,4-4*cos(M_PI/4)-4,4,-M_PI/4,0,-M_PI/2);


  // plane_obs(6-2*cos(M_PI/6)+1,6-2*cos(M_PI/6)+1,4+2*cos(M_PI/6),4,-M_PI/4,0,M_PI/6);
  // plane_obs(6-4*cos(M_PI/6)-1+1+1,6-4*cos(M_PI/6)-1+1+1,4+4*cos(M_PI/6),4,-M_PI/4,0,M_PI/2);
  // plane_obs(6-4*cos(M_PI/6)-3-1+2,6-4*cos(M_PI/6)-3-1+2,4+4*cos(M_PI/6),4,-M_PI/4,0,M_PI/2);
  // plane_obs(6-4*cos(M_PI/6)-3-3-1+3,6-4*cos(M_PI/6)-3-3-1+3,4+4*cos(M_PI/6),4,-M_PI/4,0,M_PI/2);
  // plane_obs(6-4*cos(M_PI/6)-3-3-3-1+4,6-4*cos(M_PI/6)-3-3-3-1+4,4+4*cos(M_PI/6),4,-M_PI/4,0,M_PI/2);
  // plane_obs(6-6*cos(M_PI/6)-3-3-3-1+4,6-6*cos(M_PI/6)-3-3-3-1+4,4+2*cos(M_PI/6),4,-M_PI/4,0,-M_PI/6);
  // plane_obs(6-6*cos(M_PI/6)-3-3-3-1+4-1,6-6*cos(M_PI/6)-3-3-3-1+4-1,4+2*cos(M_PI/6)-4,4,-M_PI/4,0,0);
  // //
  // //
  // plane_obs(6-2*cos(M_PI/6)+1,6-2*cos(M_PI/6)+1,-2*cos(M_PI/6),4,-M_PI/4,0,-M_PI/6);
  // plane_obs(6-4*cos(M_PI/6)-1+1+1,6-4*cos(M_PI/6)-1+1+1,-4*cos(M_PI/6),4,-M_PI/4,0,-M_PI/2);
  // plane_obs(6-4*cos(M_PI/6)-3-1+2,6-4*cos(M_PI/6)-3-1+2,-4*cos(M_PI/6),4,-M_PI/4,0,-M_PI/2);
  // plane_obs(6-4*cos(M_PI/6)-3-3-1+3,6-4*cos(M_PI/6)-3-3-1+3,-4*cos(M_PI/6),4,-M_PI/4,0,-M_PI/2);
  // plane_obs(6-4*cos(M_PI/6)-3-3-3-1+4,6-4*cos(M_PI/6)-3-3-3-1+4,-4*cos(M_PI/6),4,-M_PI/4,0,-M_PI/2);
  // plane_obs(6-6*cos(M_PI/6)-3-3-3-1+4-0.5,6-6*cos(M_PI/6)-3-3-3-1+4-0.5,-2*cos(M_PI/6),4,-M_PI/4,0,M_PI/6);


  // plane_obs(6-6*cos(M_PI/6)-3-3-3-1+4-1,6-6*cos(M_PI/6)-3-3-3-1+4-1,-2*cos(M_PI/6)-4,4,-M_PI/4,0,0);

  // cube_obs(6,6,2,4,M_PI/4);

  // spherical_obs(0,0,0,8);

  // Half U Trap
  // plane_obs(2+6,2+6,2,4,-M_PI/4,0,0);
  // plane_obs(2+6-2*cos(M_PI/6)+1,2+6-2*cos(M_PI/6)+1,4+2*cos(M_PI/6),4,-M_PI/4,0,M_PI/6);
  // plane_obs(2+6-4*cos(M_PI/6)-1+1,2+6-4*cos(M_PI/6)-1+1,4+4*cos(M_PI/6),4,-M_PI/4,0,M_PI/2);
  // plane_obs(2+6-4*cos(M_PI/6)-3-1+2,2+6-4*cos(M_PI/6)-3-1+2,4+4*cos(M_PI/6),4,-M_PI/4,0,M_PI/2);
  // plane_obs(2+6-4*cos(M_PI/6)-3-3-1+3,2+6-4*cos(M_PI/6)-3-3-1+3,4+4*cos(M_PI/6),4,-M_PI/4,0,M_PI/2);
  // plane_obs(2+6-4*cos(M_PI/6)-3-3-3-1+4,2+6-4*cos(M_PI/6)-3-3-3-1+4,4+4*cos(M_PI/6),4,-M_PI/4,0,M_PI/2);
  // plane_obs(2+6-6*cos(M_PI/6)-3-3-3-1+4,2+6-6*cos(M_PI/6)-3-3-3-1+4,4+2*cos(M_PI/6),4,-M_PI/4,0,-M_PI/6);
  // plane_obs(2+6-6*cos(M_PI/6)-3-3-3-1+4-1,2+6-6*cos(M_PI/6)-3-3-3-1+4-1,4+2*cos(M_PI/6)-4,4,-M_PI/4,0,0);

  // Sharp corner
  // double corn = M_PI*0.4;
  // plane_obs(6,6,0-4*cos(corn),8,-M_PI/4,0,-corn);
  // plane_obs(6,6,0+4*cos(corn),8,-M_PI/4,0,corn);

  // // Parallel plane
  // float spacings = 5.0;
  // plane_obs(6,6,-spacings/2,8,-M_PI/4,0,M_PI/2);
  // plane_obs(6,6,spacings/2,8,-M_PI/4,0,M_PI/2);


  // plane_obs(6,6,-4,4,-M_PI/4,0,0);
  // plane_obs(6,6,-8,4,-M_PI/4,0,0);

  // // Long Planes
  // for(char i=-3; i<=3; i++)
  // {
  //   plane_obs(6,6,8*i,8,-M_PI/4,0,0);
  // }

  // // U-Shaped
  // plane_obs(2+6,2+6,1,8,-M_PI/4,0,0);
  // plane_obs(2+6-4*cos(M_PI/4)+1,2+6-4*cos(M_PI/4)+1,4+4*cos(M_PI/4),8,-M_PI/4,0,M_PI/4);
  // plane_obs(2+6-8*cos(M_PI/4)-1+1,2+6-8*cos(M_PI/4)-1+1,4+8*cos(M_PI/4),8,-M_PI/4,0,3*M_PI/2);
  // plane_obs(2+6-4*cos(M_PI/4)+1,2+6-4*cos(M_PI/4)+1,-4-4*cos(M_PI/4),8,-M_PI/4,0,-M_PI/4);
  // plane_obs(2+6-8*cos(M_PI/4)-1+1,2+6-8*cos(M_PI/4)-1+1,-4-8*cos(M_PI/4),8,-M_PI/4,0,-3*M_PI/2);

  // // Sharp
  // double ang_corn = 0.75*M_PI;
  // double side_length = 12;
  // double ang_yaw = M_PI/4;
  // plane_obs(8,8,1,side_length,-ang_yaw,0,0);
  // plane_obs(8,8,1+side_length,side_length,-ang_yaw,0,0);
  // plane_obs(8,8,1-side_length,side_length,-ang_yaw,0,0);
  // plane_obs(8-side_length/2*sin(ang_corn)*cos(ang_yaw),8-side_length/2*sin(ang_corn)*sin(ang_yaw),1+3*side_length/2+side_length/2*cos(ang_corn),side_length,-ang_yaw,0,ang_corn);
  // plane_obs(8-side_length/2*sin(M_PI-ang_corn)*cos(ang_yaw),8-side_length/2*sin(M_PI-ang_corn)*sin(ang_yaw),1-3*side_length/2+side_length/2*cos(M_PI-ang_corn),side_length,-ang_yaw,0,-ang_corn);

  // 2D Obs
  // line_obs(9.0+4*sin(M_PI/3),0.0+4*cos(M_PI/3),8.0,M_PI/6);
  // line_obs(9.0+4*sin(M_PI/3),0.0-4*cos(-M_PI/3),8.0,-M_PI/6);
  //
  // line_obs(5.0,4.0,8.0,0);
  // line_obs(1.0,8.0,8.0,M_PI/2);
  //
  // line_obs(5.0,-4.0,8.0,0);
  // line_obs(1.0,-8.0,8.0,M_PI/2);

  // // 2D Circular Arc
  // arc_circle_obs(7, 0, 6, M_PI);

  // 2D square
  // line_obs(4.0,0.0,8.0,M_PI/2);
  // line_obs(12.0,0.0,8.0,M_PI/2);
  // line_obs(8,4.0,8.0,0);
  // line_obs(8,-4.0,8.0,0);

  // // Parallel lines
  // float spacings = 4.0;
  // line_obs(8,spacings/2,8.0,0);
  // line_obs(8,-spacings/2,8.0,0);


  // 3D tree
  // cylinder(15,0,1.5,0.5,3);
  // spherical_obs(15,0,3.5,1.0);

  // 3D forest
  // forest_3d();

  // 2D forest
  // forest_2d();

  // plane_obs(6,6,4,8,-M_PI/4,0,0);
  // plane_obs(6-2,6-2,8+4,8,-M_PI/4,0,M_PI/6);

  // plane_obs(6+1.5,6+1.5,4,4,-M_PI/4,0,-M_PI/2);

  // plane_obs(6,6,2,4,-M_PI/4,0,0);

  ros::Rate loop_rate(100);
  double begin = ros::Time::now().toSec();

  while (nh.ok())
  {

    // msg->header.stamp = ros::Time::now().toNSec();
    msg->points.clear();
    msg->width = 0;
    agent_pose_array();
    agent_obs(2);

    // double secs =ros::Time::now().toSec();
    // cout << secs-begin << endl;
    // spherical_obs(4-0.2*(secs-begin), 5, 0, 2);

    pcl_conversions::toPCL(ros::Time::now(), msg->header.stamp);
    pub.publish (msg);
    ros::spinOnce ();
    loop_rate.sleep ();
  }
}
