#include <ros/ros.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <sensor_msgs/PointCloud2.h>
#include <nav_msgs/OccupancyGrid.h>
#include <pcl_conversions/pcl_conversions.h>
#include <iostream>
#include <math.h>
#include <armadillo>
#include <tf/transform_listener.h>
using namespace std;
using namespace arma;

typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;
PointCloud::Ptr msg (new PointCloud);
float pose_map_x, pose_map_y;

void map_callback(const nav_msgs::OccupancyGrid::ConstPtr& message_)
{
  nav_msgs::OccupancyGrid variable;
  variable = *message_;

  cout << variable.info << endl;
  int cols = variable.info.width;
  int rows = variable.info.height;
  float resolution = variable.info.resolution;

  msg->points.clear();
  int index_map=0;
  int index_filled = 0;
  for(int i=0; i<rows; i++)
  {
    // cout << "o" << endl;
    for(int j=0; j<cols; j++)
    {
      if(variable.data[index_map]>0)
      {
        msg->points.push_back (pcl::PointXYZ(resolution*(float)(j)+pose_map_x,resolution*(float)(i)+pose_map_y,0));
        index_filled++;
        // cout << "x: " << resolution*(float)(j) << endl;
        // cout << "y: " << resolution*(float)(i) << endl;
        // cout << variable.data[index_map] << e
      }
      index_map++;
      // cout << index_map << endl;
      // cout << i << endl;
      // cout << j << endl;
    }
  }
  msg->width = index_filled++;
  // cout << "index_map " << index_map << endl;
}

int main(int argc, char** argv)
{
  ros::init (argc, argv, "map_to_pcd");
  ros::NodeHandle nh;
  ros::Subscriber sub = nh.subscribe<nav_msgs::OccupancyGrid>("map", 10, map_callback);
  ros::Publisher pub = nh.advertise<PointCloud> ("obstacle_point", 10);

  msg->header.frame_id = "world";
  msg->height = 1;
  tf::TransformListener listener;

  ros::Rate loop_rate(100);
  while (nh.ok())
  {
    tf::StampedTransform transform;
    try{
      listener.lookupTransform("/world", "/map",
                               ros::Time(0), transform);
    }
    catch (tf::TransformException &ex) {
      ROS_ERROR("%s",ex.what());
      ros::Duration(1.0).sleep();
      continue;
    }
    pose_map_x = transform.getOrigin().x();
    pose_map_y = transform.getOrigin().y();

    // msg->header.stamp = ros::Time::now().toNSec();
    pcl_conversions::toPCL(ros::Time::now(), msg->header.stamp);
    pub.publish (msg);
    ros::spinOnce ();
    loop_rate.sleep ();
  }
}
