#include <ros/ros.h>
#include <tf2_sensor_msgs/tf2_sensor_msgs.h>
#include <sensor_msgs/PointCloud2.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/Transform.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Quaternion.h>
#include <std_msgs/Header.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/conversions.h>
#include <pcl_ros/transforms.h>
#include <boost/foreach.hpp>
#include <pcl/PCLPointCloud2.h>

sensor_msgs::PointCloud2 cloud_in, cloud_out;
geometry_msgs::TransformStamped transform;
int flag_cloud, flag_switch;
// typedef pcl::PointCloud<pcl::PointXYZ> output_pcl, output1_pcl, output2_pcl, output3_pcl;
pcl::PCLPointCloud2 output_pcl, output1_pcl, output2_pcl, output3_pcl;

void callback(const sensor_msgs::PointCloud2::ConstPtr& msg)
{
  cloud_in = *msg;
  flag_cloud = 1;
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "pcd_transform");
  ros::NodeHandle nh;
  ros::Subscriber sub = nh.subscribe<sensor_msgs::PointCloud2>("obstacle_point_map", 10, callback);
  ros::Publisher pub = nh.advertise<sensor_msgs::PointCloud2> ("obstacle_point", 10);
  ros::Rate loop_rate(100);
  // For mount.pcd
  // transform.transform.translation.x = 30.0;
  // transform.transform.translation.y = 0.0;
  // transform.transform.translation.z = 0.0;
  // For tree.pcd and house.pcd and castle.pcd and people.pcd
  // transform.transform.translation.x = 35.0;
  // transform.transform.translation.y = 0.0;
  // transform.transform.translation.z = 0.0;

  // // For tree new
  // transform.transform.translation.x = 15.0;
  // transform.transform.translation.y = 0.0;
  // transform.transform.translation.z = 0.0;

  // For tree UAV
  transform.transform.translation.x = 5.0;
  transform.transform.translation.y = 0.0;
  transform.transform.translation.z = -0.6;

  // For Hogwarts..pcd
  // transform.transform.translation.x = 50.0;
  // transform.transform.translation.y = 0.0;
  // transform.transform.translation.z = 0.0;

  // For Baxter table1
  // transform.transform.translation.x = 0.3;
  // transform.transform.translation.y = 0.5;
  // transform.transform.translation.z = -1.1;
  // transform.transform.rotation.x = 0.7071;
  // transform.transform.rotation.y = 0.0;
  // transform.transform.rotation.z = 0.0;
  // transform.transform.rotation.w = 0.7071;

  // // For Baxter table2 sim
  // transform.transform.translation.x = 0.5;
  // transform.transform.translation.y = -0.1;
  // transform.transform.translation.z = -1.1;
  // transform.transform.rotation.x = -0.5;
  // transform.transform.rotation.y = 0.5;
  // transform.transform.rotation.z = 0.5;
  // transform.transform.rotation.w = -0.5;

  // For Baxter table2 real
  // transform.transform.translation.x = 0.5;
  // transform.transform.translation.y = -0.1;
  // transform.transform.translation.z = -0.9;
  // transform.transform.rotation.x = -0.5;
  // transform.transform.rotation.y = 0.5;
  // transform.transform.rotation.z = 0.5;
  // transform.transform.rotation.w = -0.5;

  // // For Baxter table2 real new
  // transform.transform.translation.x = 0.7;
  // transform.transform.translation.y = 0.1;
  // transform.transform.translation.z = -0.6;
  // transform.transform.rotation.x = -0.5;
  // transform.transform.rotation.y = 0.5;
  // transform.transform.rotation.z = 0.5;
  // transform.transform.rotation.w = -0.5;

  // For Baxter chair exp
  // transform.transform.translation.x = 0.0;
  // transform.transform.translation.y = 0.0;
  // transform.transform.translation.z = 0.0;
  // transform.transform.rotation.x = 0.0;
  // transform.transform.rotation.y = 0.0;
  // transform.transform.rotation.z = 0.0;
  // transform.transform.rotation.w = 1.0;

  // // For Baxter table2 real side
  // transform.transform.translation.x = 0.5;
  // transform.transform.translation.y = 0.7;
  // transform.transform.translation.z = -0.5;
  //
  // transform.transform.rotation.x = 0.7071;
  // transform.transform.rotation.y = 0.0;
  // transform.transform.rotation.z = 0.0;
  // transform.transform.rotation.w = 0.7071;

  transform.transform.rotation.x = 0.7071;
  transform.transform.rotation.y = 0.0;
  transform.transform.rotation.z = 0.0;
  transform.transform.rotation.w = 0.7071;

  // // For people2.pcd
  // transform.transform.translation.x = 5.0;
  // transform.transform.translation.y = 0.0;
  // transform.transform.translation.z = 0.0;
  // transform.transform.rotation.x = 0.7071;
  // transform.transform.rotation.y = 0.0;
  // transform.transform.rotation.z = 0.0;
  // transform.transform.rotation.w = 0.7071;

  transform.header.frame_id = "map";

  flag_switch = 0;
  sensor_msgs::PointCloud2 output_sent;
  while (nh.ok())
  {

      if(flag_cloud == 1 && flag_switch == 0)
      {
        flag_switch = 1;

        // // For a lot of tree.pcd
        // transform.transform.translation.x = 35.0;
        // transform.transform.translation.y = 0.0;
        // transform.transform.translation.z = 0.0;
        //
        // tf2::doTransform (cloud_in, cloud_out, transform);
        // pcl::concatenatePointCloud (output_sent, cloud_out, output_sent);
        //
        // transform.transform.translation.x = 45.0;
        // transform.transform.translation.y = 0.0;
        // transform.transform.translation.z = 0.0;
        //
        // tf2::doTransform (cloud_in, cloud_out, transform);
        // pcl::concatenatePointCloud (output_sent, cloud_out, output_sent);
        //
        // transform.transform.translation.x = 55.0;
        // transform.transform.translation.y = 0.0;
        // transform.transform.translation.z = 0.0;
        //
        // tf2::doTransform (cloud_in, cloud_out, transform);
        // pcl::concatenatePointCloud (output_sent, cloud_out, output_sent);

        // For a lot of people.pcd
        // transform.transform.translation.x = 35.0;
        // transform.transform.translation.y = 0.0;
        // transform.transform.translation.z = 0.0;
        //
        // tf2::doTransform (cloud_in, cloud_out, transform);
        // pcl::concatenatePointCloud (output_sent, cloud_out, output_sent);
        //
        // transform.transform.translation.x = 36.0;
        // transform.transform.translation.y = 2.0;
        // transform.transform.translation.z = 0.0;
        //
        // tf2::doTransform (cloud_in, cloud_out, transform);
        // pcl::concatenatePointCloud (output_sent, cloud_out, output_sent);

        // transform.transform.translation.x = 55.0;
        // transform.transform.translation.y = 0.0;
        // transform.transform.translation.z = 0.0;
        //
        tf2::doTransform (cloud_in, cloud_out, transform);
        // pcl::concatenatePointCloud (output_sent, cloud_out, output_sent);

      }
      if(flag_switch == 1)
      {
        // pub.publish(output_sent);
        pub.publish(cloud_out);
      }
    ros::spinOnce();
    loop_rate.sleep ();
  }

}
