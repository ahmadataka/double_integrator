#include <ros/ros.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <boost/foreach.hpp>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Vector3.h>
#include <std_msgs/Int32.h>
#include <iostream>
#include <armadillo>
#include <math.h>
using namespace std;
using namespace arma;

typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;
geometry_msgs::PoseArray obs_pose;
unsigned int obs_length;
// unsigned int obs_index;
unsigned int obs_index[3];
unsigned int obs_index3;
geometry_msgs::Vector3 obs_sent, obs_sent_new;
geometry_msgs::Vector3 robot_pose;
geometry_msgs::Vector3 obs_avg_sent;
geometry_msgs::Vector3 obs_sur_sent;
int flag_obs;
mat normal_before;
// float minim;
float minim[3] = {100, 100, 100};
float minim3;
mat pose_obs_save;
mat obs_other[3];
mat pose_obs_other_save[2];

void callback(const PointCloud::ConstPtr& msg)
{
  obs_length = (msg->width)*(msg->height);
  obs_pose.poses.clear();
  BOOST_FOREACH (const pcl::PointXYZ& pt, msg->points)
  {
    geometry_msgs::Pose obs;
    obs.position.x = pt.x;
    obs.position.y = pt.y;
    obs.position.z = pt.z;
    obs_pose.poses.push_back(obs);
  }

  flag_obs = 1;
  // cout << "length: " << obs_length << endl;
}

float magni3(geometry_msgs::Pose obs_)
{
  return pow(pow(obs_.position.x,2)+pow(obs_.position.y,2)+pow(obs_.position.z,2),0.5);
  // return pow(pow(obs_.position.x,2)+pow(obs_.position.y,2),0.5);

}

float magni2(geometry_msgs::Pose obs_)
{
  return pow(pow(obs_.position.x,2)+pow(obs_.position.y,2),0.5);

}

void closest_obs()
{
  minim3 = 100;
  minim[0] = 100; minim[1] = 100; minim[2] = 100;
  float avg_x = 0;
  float avg_y = 0;
  float avg_z = 0;
  for(unsigned int i=0; i<obs_length; i++)
  {
    avg_x = avg_x + obs_pose.poses[i].position.x;
    avg_y = avg_y + obs_pose.poses[i].position.y;
    avg_z = avg_z + obs_pose.poses[i].position.z;

    // if(magni2(obs_pose.poses[i])<minim[0])
    // {
    //   // To avoid detecting floor
    //   if(obs_pose.poses[i].position.y<0.1)
    //   {
    //     minim[2] = minim[1];
    //     minim[1] = minim[0];
    //     minim[0] = magni2(obs_pose.poses[i]);
    //     obs_index[2] = obs_index[1];
    //     obs_index[1] = obs_index[0];
    //     obs_index[0] = i;
    //   }
    // }
    // else if(magni2(obs_pose.poses[i])<minim[1] && magni2(obs_pose.poses[i])!= minim[0])
    // {
    //   // To avoid detecting floor
    //   if(obs_pose.poses[i].position.y<0.1)
    //   {
    //     minim[2] = minim[1];
    //     minim[1] = magni2(obs_pose.poses[i]);
    //     obs_index[2] = obs_index[1];
    //     obs_index[1] = i;
    //   }
    // }
    // else if(magni2(obs_pose.poses[i])<minim[2] && magni2(obs_pose.poses[i])!= minim[1])
    // {
    //   // To avoid detecting floor
    //   if(obs_pose.poses[i].position.y<0.1)
    //   {
    //     minim[2] = magni2(obs_pose.poses[i]);
    //     obs_index[2] = i;
    //   }
    // }

    if(magni3(obs_pose.poses[i])<minim3)
    {
      // To avoid detecting floor
      if(obs_pose.poses[i].position.y<0.1)
      {
        minim3 = magni3(obs_pose.poses[i]);
        obs_index3 = i;
      }
    }
    // cout << "dist:" << magni(obs_pose.poses[i],robot_pose) << endl;
    // cout << "obs" << i << ": " << obs_pose.poses[i].position << endl;
    // cout << "rob:" << robot_pose << endl;
  }

  avg_x = avg_x / obs_length;
  avg_y = avg_y / obs_length;
  avg_z = avg_z / obs_length;
  obs_avg_sent.x = avg_x;
  obs_avg_sent.y = avg_y;
  obs_avg_sent.z = avg_z;

  mat obs_bound;
  double obs_lim=0.0;
  // obs_bound << obs_pose.poses[obs_index].position.x << endr
  //           << obs_pose.poses[obs_index].position.y << endr
  //           << obs_pose.poses[obs_index].position.z << endr;
  obs_bound << obs_pose.poses[obs_index3].position.x << endr
            << obs_pose.poses[obs_index3].position.y << endr
            << obs_pose.poses[obs_index3].position.z << endr;
  obs_bound = (norm(obs_bound)-obs_lim)*obs_bound/norm(obs_bound);
  // obs_sent.x = obs_pose.poses[obs_index[0]].position.x - robot_pose.x;
  // obs_sent.y = obs_pose.poses[obs_index[0]].position.y - robot_pose.y;
  // obs_sent.z = obs_pose.poses[obs_index[0]].position.z - robot_pose.z;
  obs_sent.x = obs_bound(0,0);
  obs_sent.y = obs_bound(1,0);
  obs_sent.z = obs_bound(2,0);

  // obs_other[0] << obs_pose.poses[obs_index[1]].position.x << endr
  //              << obs_pose.poses[obs_index[1]].position.y << endr
  //              << obs_pose.poses[obs_index[1]].position.z << endr
  //              << 1 << endr;
  // obs_other[1] << obs_pose.poses[obs_index[2]].position.x << endr
  //              << obs_pose.poses[obs_index[2]].position.y << endr
  //              << obs_pose.poses[obs_index[2]].position.z << endr
  //              << 1 << endr;
  // obs_other[2] << obs_pose.poses[obs_index[0]].position.x << endr
  //              << obs_pose.poses[obs_index[0]].position.y << endr
  //              << obs_pose.poses[obs_index[0]].position.z << endr
  //              << 1 << endr;
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "sub_point");
  ros::NodeHandle nh;
  char flag = 0;
  char flag2 = 0;
  std_msgs::Int32 flag_out_of_range;
  flag_obs = 0;
  normal_before.zeros(3,1);
  // nh.param("robot", turtle_name, turtle_name);

  ros::Subscriber sub = nh.subscribe<PointCloud>("/camera/depth/points", 10, callback);
  ros::Publisher pub = nh.advertise<geometry_msgs::PoseArray> ("obstacle_poses", 10);
  ros::Publisher pub_dist = nh.advertise<geometry_msgs::Vector3> ("agent/dist_to_obs", 10);
  ros::Publisher pub_obs = nh.advertise<geometry_msgs::Vector3> ("obs_central", 10);
  ros::Publisher pub_obs_sur = nh.advertise<geometry_msgs::Vector3> ("obs_surface", 10);
  ros::Publisher pub_obs_range = nh.advertise<std_msgs::Int32> ("out_of_range", 10);
  tf::TransformListener listener, listener_robot;
  ros::Rate loop_rate(100);

  obs_sent.x = 100; obs_sent.y = 100; obs_sent.z = 100;
  pose_obs_save << 100.0 << endr
                << 100.0 << endr
                << 100.0 << endr;


  for(unsigned char i=0; i<3; i++)
  {
    pose_obs_other_save[i] << 100.0 << endr
                           << 100.0 << endr
                           << 100.0 << endr;
    obs_other[i] << 100 << endr
                 << 100 << endr
                 << 100 << endr
                 << 1 << endr;
  }
  while (nh.ok())
  {
    tf::StampedTransform transform, transform_robot;
      try
      {
        listener.lookupTransform("/odom", "/camera_depth_optical_frame",
                                 ros::Time(0), transform);
        flag = 1;
      }
      catch (tf::TransformException ex)
      {
        ROS_ERROR("%s",ex.what());
        // ros::Duration(1.0).sleep();
      }

      if(flag == 1 and flag_obs == 1)
      {

        // robot_pose.x = transform.getOrigin().x();
        // robot_pose.y = transform.getOrigin().y();
        // robot_pose.z = transform.getOrigin().z();

        closest_obs();


        mat homo_tf, pose_obs, pose_obs_odom, pose_obs_other[3];
        // cout << minim << endl;

        // if(minim[0]!=100)
        if(minim[0]<4 || minim3 <4)
        {
          tf::Quaternion q(transform.getRotation().x(), transform.getRotation().y(), transform.getRotation().z(), transform.getRotation().w());
          tf::Matrix3x3 m(q);

          homo_tf << m[0][0] << m[0][1] << m[0][2] << transform.getOrigin().x() << endr
                  << m[1][0] << m[1][1] << m[1][2] << transform.getOrigin().y() << endr
                  << m[2][0] << m[2][1] << m[2][2] << transform.getOrigin().z() << endr
                  << 0.0 << 0.0 << 0.0 << 1.0 << endr;
          pose_obs << obs_sent.x << endr
                   << obs_sent.y << endr
                   << obs_sent.z << endr
                   << 1 << endr;
          pose_obs_odom = homo_tf*pose_obs;
          pose_obs_save = pose_obs_odom;
          // cout << obs_sent << endl;

          // for(unsigned char i=0; i<3; i++)
          // {
          //   pose_obs_other[i] = homo_tf*obs_other[i];
          //   pose_obs_other_save[i] = pose_obs_other[i];
          // }
          flag_out_of_range.data = 0;
        }
        else
        {
          // pose_obs_odom << 100.0 << endr
          //               << 100.0 << endr
          //               << 100.0 << endr;

          pose_obs_odom = pose_obs_save;
          flag_out_of_range.data = 1;

          // for(unsigned char i=0; i<3; i++)
          // {
          //   pose_obs_other[i] =   pose_obs_other_save[i];
          // }

        }

        // for(unsigned char i=0; i<3; i++)
        // {
        //   pose_obs_other[i].print("pose_obs_other:");
        // }
        // double grad, obs_sum_xy, obs_avg_x, obs_avg_y, obs_sum_x2, num;
        // num = 3; obs_sum_xy = 0; obs_avg_x = 0; obs_avg_y = 0; obs_sum_x2 = 0;
        // for(unsigned char i=0; i<3; i++)
        // {
        //   obs_sum_xy = obs_sum_xy + pose_obs_other[i](0,0)*pose_obs_other[i](1,0);
        //   obs_sum_x2 = obs_sum_x2 + pose_obs_other[i](0,0)*pose_obs_other[i](0,0);
        //   obs_avg_x = obs_avg_x + pose_obs_other[i](0,0);
        //   obs_avg_y = obs_avg_y + pose_obs_other[i](1,0);
        //   // pose_obs_other.print("")
        // }
        //
        // obs_avg_y = obs_avg_y/num; obs_avg_x = obs_avg_x/num;
        // grad = (obs_sum_xy - num*obs_avg_x*obs_avg_y)/(obs_sum_x2 - num*obs_avg_x*obs_avg_x);
        // mat obs_surf;
        // obs_surf << 1/pow(1+grad*grad,0.5) << endr
        //          << grad/pow(1+grad*grad,0.5) << endr;
        // if(isnan(obs_surf(0,0))==0 and isnan(obs_surf(1,0))==0)
        // {
        //   // obs_surf.print("sur");
        //   obs_sur_sent.x = obs_surf(0,0);
        //   obs_sur_sent.y = obs_surf(1,0);
        //
        //   // coba
        //   // obs_sur_sent.x = 0.0;
        //   // obs_sur_sent.y = 1.0;
        // }



        // pose_obs_odom.print("obs:");
        // if(minim[0]<8 || minim3 <8)
        // {


        static tf::TransformBroadcaster br;
        tf::Transform tr;
        // tr.setOrigin( tf::Vector3(pose_obs_other[2](0,0), pose_obs_other[2](1,0), pose_obs_other[2](2,0)) );
        tr.setOrigin( tf::Vector3(pose_obs_odom(0,0), pose_obs_odom(1,0), pose_obs_odom(2,0)) );
        // tr.setOrigin( tf::Vector3(pose_obs(0,0), pose_obs(1,0), pose_obs(2,0)) );
        tf::Quaternion q_new;
        q_new.setRPY(0, 0, 0);
        tr.setRotation(q_new);
        br.sendTransform(tf::StampedTransform(tr, ros::Time::now(), "odom", "closest_obs"));
        //
        // tr.setOrigin( tf::Vector3(pose_obs_other[0](0,0), pose_obs_other[0](1,0), pose_obs_other[0](2,0)) );
        // // tr.setOrigin( tf::Vector3(pose_obs(0,0), pose_obs(1,0), pose_obs(2,0)) );
        //
        // q_new.setRPY(0, 0, 0);
        // tr.setRotation(q_new);
        // br.sendTransform(tf::StampedTransform(tr, ros::Time::now(), "odom", "closest_obs_2"));
        //
        // tr.setOrigin( tf::Vector3(pose_obs_other[1](0,0), pose_obs_other[1](1,0), pose_obs_other[1](2,0)) );
        // // tr.setOrigin( tf::Vector3(pose_obs(0,0), pose_obs(1,0), pose_obs(2,0)) );
        //
        // q_new.setRPY(0, 0, 0);
        // tr.setRotation(q_new);
        // br.sendTransform(tf::StampedTransform(tr, ros::Time::now(), "odom", "closest_obs_3"));

        obs_sent_new.x = pose_obs_odom(0,0) - transform.getOrigin().x();
        obs_sent_new.y = pose_obs_odom(1,0) - transform.getOrigin().y();
        obs_sent_new.z = pose_obs_odom(2,0) - transform.getOrigin().z();
        // }
        // else
        // {
        //   obs_sent_new.x = 100;
        //   obs_sent_new.y = 100;
        //   obs_sent_new.z = 100;
        // }
      }

      // pub.publish(obs_pose);
      pub_dist.publish(obs_sent_new);
      pub_obs_range.publish(flag_out_of_range);
      // pub_obs_sur.publish(obs_sur_sent);
      // pub_obs.publish(obs_avg_sent);

    ros::spinOnce();
    // loop_rate.sleep();
  }

}
