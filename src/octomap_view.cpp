#include <ros/ros.h>
#include <octomap_msgs/conversions.h>
#include <octomap_msgs/Octomap.h>
#include <octomap_msgs/GetOctomap.h>
#include <octomap_ros/conversions.h>
#include <iostream>
using namespace std;

void print_query_info(octomap::point3d query, octomap::OcTreeNode* node) {
  if (node != NULL) {
    cout << "occupancy probability at " << query << ":\t " << node->getOccupancy() << endl;
  }
  else
    cout << "occupancy probability at " << query << ":\t is unknown" << endl;
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "octomap_read");
  ros::AsyncSpinner spinner(1);
  spinner.start();
  ros::NodeHandle nh;

  ros::Publisher octomap_publisher = nh.advertise<octomap_msgs::Octomap>("octomap",1);
  char path[1024]; getcwd(path, sizeof(path)); ROS_INFO("Current path is [%s]", path);
  octomap::OcTree* octree = new octomap::OcTree("/home/ahmadataka/catkin_ws/src/double_integrator/world/house.binvox.bt");

  octomap_msgs::Octomap bmap_msg;
  octomap_msgs::binaryMapToMsg(*octree, bmap_msg);

  bmap_msg.header.seq = 0;
  bmap_msg.header.stamp = ros::Time::now();
  bmap_msg.header.frame_id = "world";
  ros::Rate loop_rate(100);

  cout << bmap_msg << endl;
  while (nh.ok())
  {
    octomap_publisher.publish(bmap_msg);

    octomap::point3d query (0., 0., 0.);
    octomap::OcTreeNode* result = octree->search (query);
    print_query_info(query, result);

      // ROS_WARN("Waiting for Subscribers");
      // ros::Duration(1.0).sleep();
      ros::spinOnce ();
      loop_rate.sleep ();
  }

  ros::shutdown();
  return 0;
}
