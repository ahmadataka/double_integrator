#include <ros/ros.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <boost/foreach.hpp>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Vector3.h>

typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;
PointCloud::Ptr new_msg (new PointCloud);
// For castle.pcd and Hogwarts.pcd
// double scaling = 0.001;
// For tree.pcd
// double scaling = 1;
//For tree.pcd UAV
double scaling = 0.3;
// For mount.pcd
// double scaling = 4;
// For city.pcd
// double scaling = 0.05;
// For people.pcd
// double scaling = 0.02;
// For people2.pcd
// double scaling = 1.5;
// For Baxter table
// double scaling = 0.015;
// For Baxter table_new
// double scaling = 0.010;
// FOr Baxter chair exp
// double scaling = 1;
int flag_scaling;
void callback(const PointCloud::ConstPtr& msg)
{
  new_msg->width = msg-> width;
  new_msg->height = msg-> height;

  BOOST_FOREACH (const pcl::PointXYZ& pt, msg->points)
  {
    new_msg->points.push_back (pcl::PointXYZ(pt.x*scaling, pt.y*scaling, pt.z*scaling));
  }
  flag_scaling = 1;
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "pcl_scaling");
  ros::NodeHandle nh;
  ros::Subscriber sub = nh.subscribe<PointCloud>("obstacle_point_raw", 10, callback);
  ros::Publisher pub = nh.advertise<PointCloud> ("obstacle_point_map", 10);
  ros::Rate loop_rate(100);
  new_msg->header.frame_id = "map";
  while (nh.ok())
  {
      if(flag_scaling == 1)
      {
        pcl_conversions::toPCL(ros::Time::now(), new_msg->header.stamp);
        pub.publish(new_msg);
      }
    ros::spinOnce();
    loop_rate.sleep ();
  }

}
