#include <ros/ros.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <boost/foreach.hpp>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Vector3.h>
#include <iostream>
#include <armadillo>
using namespace std;
using namespace arma;

typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;
geometry_msgs::PoseArray obs_pose;
unsigned int obs_length;
unsigned int obs_index[8];
geometry_msgs::Vector3 obs_sent;
geometry_msgs::Vector3 obs_sent_cand;
geometry_msgs::Vector3 robot_pose;
geometry_msgs::Vector3 obs_avg_sent;
geometry_msgs::Vector3 obs_avg_real_sent;
int flag_obs, flag_sensor;
mat normal_before;
mat obs_bound;
double obs_lim=0.2;


void callback(const PointCloud::ConstPtr& msg)
{
  obs_length = (msg->width)*(msg->height);
  obs_pose.poses.clear();
  BOOST_FOREACH (const pcl::PointXYZ& pt, msg->points)
  {
    geometry_msgs::Pose obs;
    obs.position.x = pt.x;
    obs.position.y = pt.y;
    obs.position.z = pt.z;
    obs_pose.poses.push_back(obs);
  }
  flag_obs = 1;
}

float magni(geometry_msgs::Pose obs_, geometry_msgs::Vector3 rob_)
{
  return pow(pow(obs_.position.x - rob_.x,2)+pow(obs_.position.y - rob_.y,2)+pow(obs_.position.z - rob_.z,2),0.5);
}

float dist_calculate(geometry_msgs::Vector3 vect)
{
  return pow(pow(vect.x,2)+pow(vect.y,2)+pow(vect.z,2),0.5);
}

void closest_obs()
{
  float minim[8] = {100, 100, 100, 100, 100, 100, 100, 100};
  float avg_x = 0;
  float avg_y = 0;
  float avg_z = 0;

  for(unsigned int i=0; i<obs_length; i++)
  {
    avg_x = avg_x + obs_pose.poses[i].position.x;
    avg_y = avg_y + obs_pose.poses[i].position.y;
    avg_z = avg_z + obs_pose.poses[i].position.z;

    if(magni(obs_pose.poses[i],robot_pose)<minim[0] && magni(obs_pose.poses[i],robot_pose)>=obs_lim)
    {
      minim[3] = minim[2];
      minim[2] = minim[1];
      minim[1] = minim[0];
      minim[0] = magni(obs_pose.poses[i],robot_pose);
      obs_index[3] = obs_index[2];
      obs_index[2] = obs_index[1];
      obs_index[1] = obs_index[0];
      obs_index[0] = i;
    }
    else if(magni(obs_pose.poses[i],robot_pose)<minim[1] && magni(obs_pose.poses[i],robot_pose)!= minim[0])
    {
      minim[3] = minim[2];
      minim[2] = minim[1];
      minim[1] = magni(obs_pose.poses[i],robot_pose);
      obs_index[3] = obs_index[2];
      obs_index[2] = obs_index[1];
      obs_index[1] = i;
    }
    else if(magni(obs_pose.poses[i],robot_pose)<minim[2] && magni(obs_pose.poses[i],robot_pose)!= minim[1])
    {
      minim[3] = minim[2];
      minim[2] = magni(obs_pose.poses[i],robot_pose);
      obs_index[3] = obs_index[2];
      obs_index[2] = i;
    }
    else if(magni(obs_pose.poses[i],robot_pose)<minim[1] && magni(obs_pose.poses[i],robot_pose)!= minim[0])
    {
      minim[3] = magni(obs_pose.poses[i],robot_pose);
      obs_index[3] = i;
    }
    // cout << "dist:" << magni(obs_pose.poses[i],robot_pose) << endl;
    // cout << "obs" << i << ": " << obs_pose.poses[i].position << endl;
    // cout << "rob:" << robot_pose << endl;
  }

  // Calculate the real-time average of the closest obstacle points
  obs_avg_real_sent.x = 0; obs_avg_real_sent.y = 0; obs_avg_real_sent.z = 0;
  for(unsigned char i = 0; i<4; i++)
  {
    obs_avg_real_sent.x = obs_avg_real_sent.x + obs_pose.poses[obs_index[i]].position.x;
    obs_avg_real_sent.y = obs_avg_real_sent.y + obs_pose.poses[obs_index[i]].position.y;
    obs_avg_real_sent.z = obs_avg_real_sent.z + obs_pose.poses[obs_index[i]].position.z;
  }
  obs_avg_real_sent.x = obs_avg_real_sent.x / 4 - robot_pose.x;
  obs_avg_real_sent.y = obs_avg_real_sent.y / 4 - robot_pose.y;
  obs_avg_real_sent.z = obs_avg_real_sent.z / 4 - robot_pose.z;

  // static tf::TransformBroadcaster br;
  // tf::Transform tr;
  // tr.setOrigin( tf::Vector3(obs_avg_real_sent.x, obs_avg_real_sent.y, obs_avg_real_sent.z) );
  // tf::Quaternion q_new;
  // q_new.setRPY(0, 0, 0);
  // tr.setRotation(q_new);
  // br.sendTransform(tf::StampedTransform(tr, ros::Time::now(), "agent", "avg_obs"));

  avg_x = avg_x / obs_length;
  avg_y = avg_y / obs_length;
  avg_z = avg_z / obs_length;
  obs_avg_sent.x = avg_x - robot_pose.x;
  obs_avg_sent.y = avg_y - robot_pose.y;
  obs_avg_sent.z = avg_z - robot_pose.z;
  // cout << "obs:" << obs_index[0] << " " << obs_index[1] << " " << obs_index[2] << " " << obs_index[3] << " " << endl;
  // for(unsigned char i=0; i<4; i++)
  // {
  //   cout << "obs" << int(i) << ": " << obs_pose.poses[obs_index[i]].position << endl;
  //   cout << "number:" << obs_index[i] << endl;
  // }
  // cout << "obs_length:" << obs_length << endl;
  // cout << robot_pose << endl;
  // cout << "dist_close:" << magni(obs_pose.poses[obs_index],robot_pose) << endl;
  // mat vect1, vect2, v1_cross, normal, vect_to_corn, vect_to_sur;
  // vect1 << obs_pose.poses[obs_index[1]].position.x - obs_pose.poses[obs_index[0]].position.x << endr
  //       << obs_pose.poses[obs_index[1]].position.y - obs_pose.poses[obs_index[0]].position.y << endr
  //       << obs_pose.poses[obs_index[1]].position.z - obs_pose.poses[obs_index[0]].position.z << endr;
  // vect2 << obs_pose.poses[obs_index[2]].position.x - obs_pose.poses[obs_index[0]].position.x << endr
  //       << obs_pose.poses[obs_index[2]].position.y - obs_pose.poses[obs_index[0]].position.y << endr
  //       << obs_pose.poses[obs_index[2]].position.z - obs_pose.poses[obs_index[0]].position.z << endr;
  // // vect1.print("vec1:");
  // // vect2.print("vec2:");
  // v1_cross << 0 << -vect1(2,0) << vect1(1,0) << endr
  //          << vect1(2,0) << 0 << -vect1(0,0) << endr
  //          << -vect1(1,0) << vect1(0,0) << 0 << endr;
  // normal = v1_cross*vect2;
  //
  // // normal.print("norm:");
  // double sum_x, sum_y, sum_z, sum_xy, sum_yz, sum_xz, sum_x2, sum_y2, sum_z2;
  // sum_x = 0; sum_y = 0; sum_z = 0; sum_xy = 0; sum_xz = 0; sum_yz = 0; sum_x2 = 0; sum_y2 = 0; sum_z2 = 0;
  //
  // for(unsigned char i=0; i<8; i++)
  // {
  //   sum_x = sum_x + obs_pose.poses[obs_index[i]].position.x;
  //   sum_y = sum_y + obs_pose.poses[obs_index[i]].position.y;
  //   sum_z = sum_z + obs_pose.poses[obs_index[i]].position.z;
  //   sum_xy = sum_xy + obs_pose.poses[obs_index[i]].position.x*obs_pose.poses[obs_index[i]].position.y;
  //   sum_yz = sum_yz + obs_pose.poses[obs_index[i]].position.y*obs_pose.poses[obs_index[i]].position.z;
  //   sum_xz = sum_xz + obs_pose.poses[obs_index[i]].position.x*obs_pose.poses[obs_index[i]].position.z;
  //   sum_x2 = sum_x2 + pow(obs_pose.poses[obs_index[i]].position.x,2);
  //   sum_y2 = sum_y2 + pow(obs_pose.poses[obs_index[i]].position.y,2);
  //   sum_z2 = sum_z2 + pow(obs_pose.poses[obs_index[i]].position.z,2);
  // }
  // mat A, B, C, normal_new, vect_to_sur_new;
  // A << sum_x2 << sum_xy << sum_xz << endr
  //   << sum_xy << sum_y2 << sum_yz << endr
  //   << sum_xz << sum_yz << sum_z2 << endr;
  // B << sum_x << endr
  //   << sum_y << endr
  //   << sum_z << endr;
  // // A.print("A");
  // // B.print("B");
  // // cout << "det:" << det(A) << endl;
  //
  // double mag_norm = pow((pow(normal(0,0),2)+pow(normal(1,0),2)+pow(normal(2,0),2)),0.5);
  // // cout << mag_norm << endl;
  //
  // vect_to_corn << obs_pose.poses[obs_index[0]].position.x - robot_pose.x << endr
  //              << obs_pose.poses[obs_index[0]].position.y - robot_pose.y << endr
  //              << obs_pose.poses[obs_index[0]].position.z - robot_pose.z << endr;
  // mat dot_prod;
  // // if(mag_norm!=0)
  // // {
  // //   normal = normal/mag_norm;
  // //   dot_prod = (vect_to_corn.t()*normal);
  // //   vect_to_sur = dot_prod(0,0)*normal;
  // //   // vect_to_sur.print("obs:");
  // //   // ROS_INFO("plane");
  // // }
  // // else
  // // {
  // //   dot_prod = vect_to_corn.t()*(-vect1);
  // //   vect_to_sur = vect_to_corn - dot_prod(0,0)*(-vect1)/norm(vect1);
  // //   // ROS_INFO("line");
  // //   // vect_to_sur.print("obs:");
  // // }
  // // test
  // // cout << "ok" << endl;
  // if(det(A)>0.1)
  // {
  //   C = inv(A)*B;
  //   normal_new = C;
  //   if((normal_new(0,0)!= normal_before(0,0)) and (normal_new(1,0)!= normal_before(1,0)) and (normal_new(2,0)!= normal_before(2,0)))
  //   {
  //     // cout << "change" << endl;
  //     normal_new.print("new:");
  //   }
  //   normal_before = normal_new;
  //
  //   normal_new = normal_new/norm(normal_new);
  //   dot_prod = (vect_to_corn.t()*normal_new);
  //   vect_to_sur_new = dot_prod(0,0)*normal_new;
  //
  //   // Determine the closest point on the obstacle
  //   mat closest_point, vect_to_all_corn[8];
  //   // closest_point << robot_pose.x << endr
  //   //               << robot_pose.y << endr
  //   //               << robot_pose.z << endr;
  //   // closest_point = closest_point + vect_to_sur_new;
  //   closest_point = vect_to_sur_new;
  //   // Detect whether the closest point is inside the planar
  //   for(unsigned char i =0; i<8; i++)
  //   {
  //     vect_to_all_corn[i] << obs_pose.poses[obs_index[i]].position.x - robot_pose.x << endr
  //                         << obs_pose.poses[obs_index[i]].position.y - robot_pose.y << endr
  //                         << obs_pose.poses[obs_index[i]].position.z - robot_pose.z << endr;
  //   }
  //   mat AB, AM, BC, BM, DA, DM, CD, CM;
  //   AB = vect_to_all_corn[1] - vect_to_all_corn[0];
  //   BC = vect_to_all_corn[2] - vect_to_all_corn[1];
  //   CD = vect_to_all_corn[3] - vect_to_all_corn[2];
  //   DA = vect_to_all_corn[0] - vect_to_all_corn[3];
  //   AM = closest_point - vect_to_all_corn[0];
  //   BM = closest_point - vect_to_all_corn[1];
  //   CM = closest_point - vect_to_all_corn[2];
  //   DM = closest_point - vect_to_all_corn[3];
  //   int flag_point1, flag_point2, flag_point3, flag_point4;
  //   mat dot_;
  //   dot_ = (AB.t()*AM);
  //   if((0 <= dot_(0,0)) and (dot_(0,0) <= norm(AB))) flag_point1 = 1;
  //   else flag_point1 = 0;
  //   dot_ = (BC.t()*BM);
  //   if((0 <= dot_(0,0)) and (dot_(0,0)<= norm(BC))) flag_point2 = 1;
  //   else flag_point2 = 0;
  //   dot_ = (CD.t()*CM);
  //   if((0 <= dot_(0,0)) and (dot_(0,0) <= norm(CD))) flag_point3 = 1;
  //   else flag_point3 = 0;
  //   dot_ = (DA.t()*DM);
  //   if((0 <= dot_(0,0)) and (dot_(0,0) <= norm(DA))) flag_point4 = 1;
  //   else flag_point4 = 0;
  //   if((flag_point1==1 and flag_point2==1 and flag_point3==1 and flag_point4==1))
  //   {
  //     // ROS_INFO("inside");
  //     vect_to_sur_new = vect_to_sur_new;
  //   }
  //   else
  //   {
  //     // ROS_INFO("outside");
  //     vect_to_sur_new << obs_pose.poses[obs_index[0]].position.x - robot_pose.x << endr
  //                     << obs_pose.poses[obs_index[0]].position.y - robot_pose.y << endr
  //                     << obs_pose.poses[obs_index[0]].position.z - robot_pose.z << endr;
  //   }
  //
  // }
  // else
  // {
  //   // ROS_INFO("Line");
  //   if(norm(vect1)!=0)
  //   {
  //     dot_prod = vect_to_corn.t()*(-vect1);
  //     vect_to_sur_new = vect_to_corn - dot_prod(0,0)*(-vect1)/norm(vect1);
  //   }
  //   else
  //   {
  //     vect_to_sur_new << obs_pose.poses[obs_index[0]].position.x - robot_pose.x << endr
  //                     << obs_pose.poses[obs_index[0]].position.y - robot_pose.y << endr
  //                     << obs_pose.poses[obs_index[0]].position.z - robot_pose.z << endr;
  //   }
  // }
  //
  // // if(norm(vect_to_sur_new) > norm(vect_to_corn))
  // // vect_to_sur_new.print("dist_plane:");
  // vect_to_sur = vect_to_sur_new;
  // mat to_show;
  // to_show << obs_pose.poses[obs_index[0]].position.x - robot_pose.x << endr
  //         << obs_pose.poses[obs_index[0]].position.y - robot_pose.y << endr
  //         << obs_pose.poses[obs_index[0]].position.z - robot_pose.z << endr;
  // double diff = norm(to_show) - norm(vect_to_sur);
  // cout << "diff: " << diff << endl;
  // vect_to_corn.print("dist_point:");
  // obs_sent.x = vect_to_sur(0,0);
  // obs_sent.y = vect_to_sur(1,0);
  // obs_sent.z = vect_to_sur(2,0);


  obs_bound << obs_pose.poses[obs_index[0]].position.x - robot_pose.x << endr
            << obs_pose.poses[obs_index[0]].position.y - robot_pose.y << endr
            << obs_pose.poses[obs_index[0]].position.z - robot_pose.z << endr;
  obs_bound = (norm(obs_bound)-obs_lim)*obs_bound/norm(obs_bound);
  // obs_sent.x = obs_pose.poses[obs_index[0]].position.x - robot_pose.x;
  // obs_sent.y = obs_pose.poses[obs_index[0]].position.y - robot_pose.y;
  // obs_sent.z = obs_pose.poses[obs_index[0]].position.z - robot_pose.z;
  obs_sent.x = obs_bound(0,0);
  obs_sent.y = obs_bound(1,0);
  obs_sent.z = obs_bound(2,0);
}

void closest_obs_averaging()
{
  float minim[8] = {100, 100, 100, 100, 100, 100, 100, 100};
  // float bound = 3.0;
  // float bound = 2.0;
  float bound = 1.5;
  float avg_x = 0;
  float avg_y = 0;
  float avg_z = 0;
  int obs_close=0;
  for(unsigned int i=0; i<obs_length; i++)
  {
    if(magni(obs_pose.poses[i],robot_pose)<bound && magni(obs_pose.poses[i],robot_pose)>=obs_lim)
    {
      avg_x = avg_x + obs_pose.poses[i].position.x;
      avg_y = avg_y + obs_pose.poses[i].position.y;
      avg_z = avg_z + obs_pose.poses[i].position.z;
      obs_close++;
    }
  }
  if(obs_close!=0)
  {
    avg_x = avg_x / obs_close - robot_pose.x;
    avg_y = avg_y / obs_close - robot_pose.y;
    avg_z = avg_z / obs_close - robot_pose.z;
  }
  else
  {
    avg_x = 100;
    avg_y = 100;
    avg_z = 100;
  }

  obs_sent_cand.x = avg_x;
  obs_sent_cand.y = avg_y;
  obs_sent_cand.z = avg_z;
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "sub_pcl");
  std::string ns = ros::this_node::getNamespace();
  std::string ns_tf = ns.substr(2,ns.length());

  ros::NodeHandle nh;
  char flag = 0;
  flag_obs = 0;
  normal_before.zeros(3,1);
  // nh.param("robot", turtle_name, turtle_name);
  nh.param("flag_sensor", flag_sensor, flag_sensor);
  cout << "flag_sensor " << flag_sensor << endl;
  ros::Subscriber sub = nh.subscribe<PointCloud>("/obstacle_point", 10, callback);
  ros::Publisher pub = nh.advertise<geometry_msgs::PoseArray> ("obstacle_poses", 10);
  ros::Publisher pub_dist = nh.advertise<geometry_msgs::Vector3> ("agent/dist_to_obs", 10);
  ros::Publisher pub_obs = nh.advertise<geometry_msgs::Vector3> ("obs_central", 10);
  ros::Publisher pub_obs_real = nh.advertise<geometry_msgs::Vector3> ("obs_central_real", 10);
  tf::TransformListener listener;
  ros::Rate loop_rate(100);
  while (nh.ok())
  {
    tf::StampedTransform transform;
      try
      {
        listener.lookupTransform("/world", ns_tf,
                                 ros::Time(0), transform);
        flag = 1;
      }
      catch (tf::TransformException ex)
      {
        ROS_ERROR("%s",ex.what());
        ros::Duration(1.0).sleep();
      }
      // cout << "flag_obs: " << int(flag_obs) << endl;

      if(flag == 1 and flag_obs == 1)
      {
        // ROS_INFO("tes");
        robot_pose.x = transform.getOrigin().x();
        robot_pose.y = transform.getOrigin().y();
        robot_pose.z = transform.getOrigin().z();
        closest_obs();
        closest_obs_averaging();
        if(flag_sensor == 1)
        {
          if(dist_calculate(obs_sent)>dist_calculate(obs_sent_cand))
          {
            obs_sent.x = obs_sent_cand.x;
            obs_sent.y = obs_sent_cand.y;
            obs_sent.z = obs_sent_cand.z;
          }
        }
        static tf::TransformBroadcaster br;
        tf::Transform tr;
        tr.setOrigin( tf::Vector3(obs_sent.x, obs_sent.y, obs_sent.z) );
        tf::Quaternion q_new;
        q_new.setRPY(0, 0, 0);
        tr.setRotation(q_new);
        br.sendTransform(tf::StampedTransform(tr, ros::Time::now(), "agent", "avg_new"));
        pub.publish(obs_pose);
        pub_dist.publish(obs_sent);
        pub_obs.publish(obs_avg_sent);
        pub_obs_real.publish(obs_avg_real_sent);
      }

      // pub.publish(obs_pose);
      // pub_dist.publish(obs_sent);
      // pub_obs.publish(obs_avg_sent);

    ros::spinOnce();
    loop_rate.sleep ();
  }

}
