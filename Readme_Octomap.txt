./meshconv ~/Downloads/4vd2sk31doow-farmhouse_maya16/Farmhouse\ Maya\ 2016\ Updated/farmhouse_obj.obj -c wrl -o house -vrmlver 2


./binvox house.wrl 


binvox2bt house.binvox

octovis house.binvox.bt

rosrun double_integrator octree2pcd /home/ahmadataka/catkin_ws/src/double_integrator/world/house.binvox.bt /home/ahmadataka/catkin_ws/src/double_integrator/world/house.pcd

pcl_viewer /home/ahmadataka/catkin_ws/src/double_integrator/world/house.pcd

rosrun pcl_ros pcd_to_pointcloud /home/ahmadataka/catkin_ws/src/double_integrator/world/house.pcd 0.01 _frame_id:=/world
