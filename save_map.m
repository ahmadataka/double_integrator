clear all

rosshutdown
rosinit
rostopic list

% map = rossubscriber('/map')
% scandata = map.LatestMessage

%map = rossubscriber('/obstacle_point')
map = rossubscriber('/cloud_pcd')

scandata = receive(map)
obstacle = readXYZ(scandata) %for pointclouds obs
