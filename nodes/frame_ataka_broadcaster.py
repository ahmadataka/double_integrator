#!/usr/bin/env python
import roslib
roslib.load_manifest('double_integrator')
import math
import rospy
import tf

if __name__ == '__main__':
    rospy.init_node('frame_ataka_broadcaster')
    framename = rospy.get_param('~goal')
    ns = rospy.get_namespace()
    x_goal = rospy.get_param('~x')
    y_goal = rospy.get_param('~y')
    z_goal = rospy.get_param('~z')

    br = tf.TransformBroadcaster()
    rate = rospy.Rate(100.0)
    while not rospy.is_shutdown():
      br.sendTransform((x_goal, y_goal, z_goal),
                         (0.0, 0.0, 0.0, 1.0),
                         rospy.Time.now(),
                         ns+framename,
                         "world")
      #br.sendTransform((0.0, 0.0, 0.0),
                         #(0.0, 0.0, 0.0, 1.0),
                         #rospy.Time.now(),
                         #framename,
                         #"world")
      rate.sleep()
