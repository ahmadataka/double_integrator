#!/usr/bin/env python  
import roslib
roslib.load_manifest('double_integrator')
import rospy
import math
import tf
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Vector3

sp = Twist()
stiff = Vector3()
gravity = 0.2

def speed_input(variable):
  sp.linear.x = variable.linear.x
  sp.linear.y = variable.linear.y 

def stiff_input(variable):
  stiff.x = variable.x
  stiff.y = variable.y 
  
if __name__ == '__main__':
    KPx = 0.1
    KPy = 0.1
    KD = 0.5
    rospy.init_node('ataka_listener')
    turtle_name = rospy.get_param('~robot')
    goal_name = rospy.get_param('~goal')
    listener = tf.TransformListener()

    turtle_vel = rospy.Publisher('/%s/go_to_goal' %turtle_name, Vector3, queue_size = 10)
    turtle_dist = rospy.Publisher('/%s/dist_to_goal' %turtle_name, Vector3, queue_size = 10)
    sub_speed = rospy.Subscriber('/%s/twist' %turtle_name, Twist, speed_input)
    sub_stiff = rospy.Subscriber('/stiffness', Vector3, stiff_input)
    
    rate = rospy.Rate(10.0)
    while not rospy.is_shutdown():
        try:
            (trans,rot) = listener.lookupTransform(turtle_name, goal_name, rospy.Time(0))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue
	vel_msg = Vector3()
	dist_msg = Vector3()
	KPx = stiff.x
	KPy = stiff.y
        vel_msg.x = KPx*trans[0] - KD*sp.linear.x - gravity
        vel_msg.y = KPy*trans[1] - KD*sp.linear.y
        dist_msg.x = trans[0]
        dist_msg.y = trans[1]
        
        turtle_vel.publish(vel_msg)
	turtle_dist.publish(dist_msg)
        rate.sleep()