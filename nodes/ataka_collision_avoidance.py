#!/usr/bin/env python  
import roslib
roslib.load_manifest('double_integrator')
import rospy
import math
import tf
from sympy import *
from geometry_msgs.msg import Vector3
from std_msgs.msg import String
from geometry_msgs.msg import Twist

def find_magnitude(input1, input2):
    magnitude = math.sqrt(input1 ** 2 + input2 ** 2)
    return magnitude

sp = Twist()
dist_to_goal = Vector3()

def speed_input(variable):
  sp.linear.x = variable.linear.x
  sp.linear.y = variable.linear.y 
  
def get_dist(var):
  dist_to_goal.x = var.x
  dist_to_goal.y = var.y

if __name__ == '__main__':
    KP = 0.5
    rospy.init_node('ataka_collision')
    turtle_name = rospy.get_param('~robot')
    obstacle_number = rospy.get_param('/ObstacleNumber')
    obs_name = rospy.get_param('/ObstacleName')
    field = rospy.get_param('/field')
    print(field)
    bound = 1.5
    maks = 0
    #print(bound)
    listener = tf.TransformListener()

    turtle_vel = rospy.Publisher('/%s/collision_avoidance' %turtle_name, Vector3, queue_size = 10)
    sub_speed = rospy.Subscriber('/%s/twist' %turtle_name, Twist, speed_input)
    sub_dist = rospy.Subscriber('/%s/dist_to_goal' %turtle_name, Vector3, get_dist)
    
    vel_msg = Vector3()
    vel_obs = Vector3()
    obstacle_index = String()
    constant2 = 5
    constant = 5
    active_obstacle = obstacle_number
    beta = 2
    
    flag_goal = 0
    
    rate = rospy.Rate(10.0)
    
    while not rospy.is_shutdown():
      vel_msg.x = 0;
      vel_msg.y = 0;
      dummy_obs = 0
      for i in range(0, (obstacle_number)):
	obstacle_index = ''
	obstacle_index = str(i+1)
	try:
            (trans,rot) = listener.lookupTransform(turtle_name, obs_name+obstacle_index, rospy.Time(0))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue
	#print("x= ")
	#print(trans[0])
	#print("y= ")
	#print(trans[1])
	#if (i+2) != int(turtle_name[len(turtle_name)-1]) :
	distance = find_magnitude(trans[0], trans[1])
	speed = find_magnitude(sp.linear.x, sp.linear.y)
	if(field == 0):
	  if distance > bound :
	    vel_obs.x = 0
	    vel_obs.y = 0
	  else :
	    #print('Obstacle Turtle %d\n', i+2)
	    if distance !=0 :
	      vel_obs.y = -constant*(1/distance - 1/bound)*(trans[1])/(distance ** 3)
	      vel_obs.x = -constant*(1/distance - 1/bound)*(trans[0])/(distance ** 3)
	    else :
	      print("Colliding...\n")
	elif(field == 1):
	  # Current generation on the obstacle
	  if((flag_goal == 0) and (dist_to_goal.x !=0) and (dist_to_goal.y !=0)):
	    # Initial direction of goal
	    dist_goal_vector = Matrix([[dist_to_goal.x], [dist_to_goal.y], [0]])
	    dist_goal_mag = find_magnitude(dist_to_goal.x, dist_to_goal.y)
	    dist_init = dist_goal_vector/dist_goal_mag
	    
	    # Rotate 90 degree
	    obs_cur = Matrix([[0, 1, 0], [-1, 0, 0], [0, 0, 1]])*dist_init
	    #print("obstacle_current")
	    print(obs_cur)
	    flag_goal = 1
	    
	  if distance > 2*bound :
	    vel_obs.x = 0
	    vel_obs.y = 0
	  else :
	    dummy_obs = dummy_obs + 1
	    # new magnetic-based potential	    
	    # Current generation on the robot
	    #agent_cur = Matrix([[dist_to_goal.x], [dist_to_goal.y], [0]])
	    #dist_goal = find_magnitude(dist_to_goal.x, dist_to_goal.y)
	    agent_cur = Matrix([[trans[0]], [trans[1]], [0]])
	    agent_mag = find_magnitude(trans[0], trans[1])
	    
	    agent_cur = agent_cur/(agent_mag)
	    #print("agent_current")
	    	  
	    #Magnetic Field generation
	    #print(obs_cur[1,0])
	    lc_cross = Matrix([[0, -obs_cur[2,0], obs_cur[1,0]], [obs_cur[2,0], 0, -obs_cur[0,0]], [-obs_cur[1,0], obs_cur[0,0], 0]])
	    #print(lc_cross)
	    B = lc_cross*Matrix([[trans[0]],[trans[1]],[0]])/distance**1
	  
	    #Force acting on a robot
	    la_cross = Matrix([[0, -agent_cur[2,0], agent_cur[1,0]], [agent_cur[2,0], 0, -agent_cur[0,0]], [-agent_cur[1,0], agent_cur[0,0], 0]])
	    Force = constant2*la_cross*B
	    vel_obs.x = Force[0,0]
	    vel_obs.y = Force[1,0]
	elif(field == 2):
	  #if((flag_goal == 0) and (dist_to_goal.x !=0) and (dist_to_goal.y !=0)):
	    ## Initial direction of goal
	    #dist_goal_vector = Matrix([[dist_to_goal.x], [dist_to_goal.y], [0]])
	    #dist_goal_mag = find_magnitude(dist_to_goal.x, dist_to_goal.y)
	    #dist_init = dist_goal_vector/dist_goal_mag
	    
	    ## Rotate 90 degree
	    #obs_cur = Matrix([[0, 1, 0], [-1, 0, 0], [0, 0, 1]])*dist_init
	    ##print("obstacle_current")
	    ##print(obs_cur)
	    #flag_goal = 1
	    
	  if distance > 2*bound :
	    vel_obs.x = 0
	    vel_obs.y = 0
	  else :
	    # new magnetic-based potential
	  
	    # Current generation on the robot
	    agent_cur = Matrix([[sp.linear.x], [sp.linear.y], [0]])
	    agent_cur = agent_cur/(speed)
	    #print("agent_current")
	    #print(agent_cur)
	    
	    #Magnetic Field generation
	    #print(obs_cur[1,0])
	    lc_cross = Matrix([[0, -obs_cur[2,0], obs_cur[1,0]], [obs_cur[2,0], 0, -obs_cur[0,0]], [-obs_cur[1,0], obs_cur[0,0], 0]])
	    B = lc_cross*Matrix([[sp.linear.x],[sp.linear.y],[0]])/distance**1
	  
	    #Force acting on a robot
	    la_cross = Matrix([[0, -agent_cur[2,0], agent_cur[1,0]], [agent_cur[2,0], 0, -agent_cur[0,0]], [-agent_cur[1,0], agent_cur[0,0], 0]])
	    Force = constant2*la_cross*B
	    vel_obs.x = Force[0,0]
	    vel_obs.y = Force[1,0]
	  
	else:
	  #dynamic potential field
	  
	  if(speed!=0 and distance!=0):
	    cos_theta = ((sp.linear.x*-trans[0] + sp.linear.y*-trans[1])/(speed*distance))
	    theta = math.acos(cos_theta)
	    grad_cos_theta = [(sp.linear.x*(-trans[1])**2-sp.linear.y*-trans[0]*-trans[1])/(speed*(distance**3)), (sp.linear.y*(-trans[0])**2-sp.linear.x*-trans[0]*-trans[1])/(speed*(distance**3))]
	  else:
	    theta = 0;
	    
	  #print(theta)
	  
	  if((theta > math.pi/2) and (theta <= math.pi ) and (distance!=0) and (speed!=0)):
	    vel_obs.x = constant*(-cos_theta)**(beta-1)*speed/distance*(beta*grad_cos_theta[0]-cos_theta*-trans[0]/distance**2)
	    vel_obs.y = constant*(-cos_theta)**(beta-1)*speed/distance*(beta*grad_cos_theta[1]-cos_theta*-trans[1]/distance**2)
	  else:
	    vel_obs.x = 0
	    vel_obs.y = 0
	
	if(abs(vel_obs.x)>maks):
	  maks = abs(vel_obs.x)
	  #print(maks)
	if(abs(vel_obs.y)>maks):
	  maks = abs(vel_obs.y)
	  #print(maks)
	    
	vel_msg.x = vel_msg.x + vel_obs.x
	vel_msg.y = vel_msg.y + vel_obs.y
	
      
      #print(active_obstacle)
      turtle_vel.publish(vel_msg)
      rate.sleep()