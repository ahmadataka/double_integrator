#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('double_integrator')
import rospy
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseArray
from geometry_msgs.msg import Vector3
from double_integrator.msg import RecordMulti

class record_data(object):
  def __init__(self):
    rospy.init_node('record_data')
    # robot_type = rospy.get_param('~robot_type')
    # if(robot_type==0):
    #     # self.obs_num = rospy.get_param('/ObstacleNumber')
    #     self.obs_num = 0
    # else:
    #     self.obs_num = 0
    self.agent_num = 2
    self.send = RecordMulti()
    self.goal1 = Pose()
    self.goal2 = Pose()
    self.goal3 = Pose()
    self.goal4 = Pose()
    self.robot_pose1 = Pose()
    self.robot_pose2 = Pose()
    self.robot_pose3 = Pose()
    self.robot_pose4 = Pose()

    send_pub = rospy.Publisher('/record_data', RecordMulti, queue_size = 10)
    rec_goal1 = rospy.Subscriber('/agent1/goal_pose', Vector3, self.goal_callback1)
    rec_goal2 = rospy.Subscriber('/agent2/goal_pose', Vector3, self.goal_callback2)
    rec_goal3 = rospy.Subscriber('/agent3/goal_pose', Vector3, self.goal_callback3)
    rec_goal4 = rospy.Subscriber('/agent4/goal_pose', Vector3, self.goal_callback4)
    rec_tip1 = rospy.Subscriber('/agent1/agent/pose', Pose, self.pose_callback1)
    rec_tip2 = rospy.Subscriber('/agent2/agent/pose', Pose, self.pose_callback2)
    rec_tip3 = rospy.Subscriber('/agent3/agent/pose', Pose, self.pose_callback3)
    rec_tip4 = rospy.Subscriber('/agent4/agent/pose', Pose, self.pose_callback4)

    # rec_obs_cur = rospy.Subscriber('/obstacle_current', Vector3, self.obs_cur_callback)
    # rec_ag_cur = rospy.Subscriber('/agent_current', Vector3, self.ag_cur_callback)
    # rec_obs1 = rospy.Subscriber('/obs1/pose', Pose, self.obspose_callback1)
    # rec_obs2 = rospy.Subscriber('/obs2/pose', Pose, self.obspose_callback2)
    # rec_obs3 = rospy.Subscriber('/obs3/pose', Pose, self.obspose_callback3)
    # rec_obs4 = rospy.Subscriber('/obs4/pose', Pose, self.obspose_callback4)
    # rec_obs5 = rospy.Subscriber('/obs5/pose', Pose, self.obspose_callback5)
    # rec_obs6 = rospy.Subscriber('/obs6/pose', Pose, self.obspose_callback6)
    # rec_velocity = rospy.Subscriber('/mobile_base/commands/velocity', Twist, self.command_callback)

    self.send = RecordMulti()
    frequency = 100.0
    r = rospy.Rate(frequency)

    while not rospy.is_shutdown():
    	self.transform_to_sent()
        # print self.send
    	send_pub.publish(self.send)
        r.sleep()

  def goal_callback1(self,var):
      self.goal1.position.x = var.x
      self.goal1.position.y = var.y
      self.goal1.position.z = var.z
  def goal_callback2(self,var):
      self.goal2.position.x = var.x
      self.goal2.position.y = var.y
      self.goal2.position.z = var.z
  def goal_callback3(self,var):
      self.goal3.position.x = var.x
      self.goal3.position.y = var.y
      self.goal3.position.z = var.z
  def goal_callback4(self,var):
      self.goal4.position.x = var.x
      self.goal4.position.y = var.y
      self.goal4.position.z = var.z


  def pose_callback1(self, msg):
      self.robot_pose1.position.x = msg.position.x
      self.robot_pose1.position.y = msg.position.y
      self.robot_pose1.position.z = msg.position.z
  def pose_callback2(self, msg):
      self.robot_pose2.position.x = msg.position.x
      self.robot_pose2.position.y = msg.position.y
      self.robot_pose2.position.z = msg.position.z
  def pose_callback3(self, msg):
      self.robot_pose3.position.x = msg.position.x
      self.robot_pose3.position.y = msg.position.y
      self.robot_pose3.position.z = msg.position.z
  def pose_callback4(self, msg):
      self.robot_pose4.position.x = msg.position.x
      self.robot_pose4.position.y = msg.position.y
      self.robot_pose4.position.z = msg.position.z

  def transform_to_sent(self):
    self.send.now = rospy.get_rostime()
    self.send.robot_pose.poses = []
    self.send.robot_pose.poses.append(self.robot_pose1)
    self.send.robot_pose.poses.append(self.robot_pose2)
    self.send.robot_pose.poses.append(self.robot_pose3)
    self.send.robot_pose.poses.append(self.robot_pose4)
    self.send.goal.poses = []
    self.send.goal.poses.append(self.goal1)
    self.send.goal.poses.append(self.goal2)
    self.send.goal.poses.append(self.goal3)
    self.send.goal.poses.append(self.goal4)


if __name__ == '__main__':
    try:
        record_data()
    except rospy.ROSInterruptException: pass
