#!/usr/bin/env python
import roslib
roslib.load_manifest('double_integrator')
import rospy
import math
import tf
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Vector3
from std_msgs.msg import Int32
from std_msgs.msg import Float32
from sympy import *

sp = Twist()
stiff = Vector3()
flag_obs = Int32()
dist_to_obs = Vector3()
gravity = 0.2

def skew_symm(mat_in):
    mat_out = Matrix([[0.0, -mat_in[2,0], mat_in[1,0]], [mat_in[2,0], 0.0, -mat_in[0,0]], [-mat_in[1,0], mat_in[0,0], 0.0]])
    return mat_out

def speed_input(variable):
  sp.linear.x = variable.linear.x
  sp.linear.y = variable.linear.y
  sp.linear.z = variable.linear.z

def stiff_input(variable):
  stiff.x = variable.x
  stiff.y = variable.y
  stiff.z = variable.z

def get_flag(variable):
  flag_obs.data = variable.data

def get_distobs(var):
  dist_to_obs.x = var.x
  dist_to_obs.y = var.y
  dist_to_obs.z = var.z

if __name__ == '__main__':
    # KPx = 0.04
    # KPy = 0.04
    # KPz = 0.04
    # KP0 = 0.04

    # IROS_RAL_2018
    KPx = 0.1
    KPy = 0.1
    KPz = 0.1
    KP0 = 0.1

    ns = rospy.get_namespace()
    ns_tf = ns[1:-1]

    KP_omega = 5.0
    # KD = 0.5
    KD = 2.0
    KD_speed = 1.5
    magni_bound = 2.5
    speed_limit = 1.5
    vect_goal = Matrix([[0.0], [0.0], [0.0]])
    vect_obs = Matrix([[10.0], [10.0], [10.0]])
    dist_to_obs.x = 10.0
    dist_to_obs.y = 10.0
    dist_to_obs.z = 10.0
    dist_obs = 10*(3**2)
    rospy.init_node('ataka_listener')
    turtle_name = rospy.get_param('~robot')
    goal_name = rospy.get_param('~goal')
    speed_cont = rospy.get_param('~speed_cont')
    goal_relax = rospy.get_param('/goal_rel')
    listener = tf.TransformListener()
    init_to_goal = Float32()
    turtle_vel = rospy.Publisher('%s/go_to_goal' %turtle_name, Vector3, queue_size = 10)
    turtle_goal = rospy.Publisher('goal_pose', Vector3, queue_size = 10)
    turtle_dist = rospy.Publisher('%s/dist_to_goal' %turtle_name, Vector3, queue_size = 10)
    turtle_init = rospy.Publisher('%s/initial_to_goal' %turtle_name, Float32, queue_size = 10)
    sub_speed = rospy.Subscriber('%s/twist' %turtle_name, Twist, speed_input)
    sub_stiff = rospy.Subscriber('stiffness', Vector3, stiff_input)
    sub_flag = rospy.Subscriber('flag_obstacle', Int32, get_flag)
    sub_distobs = rospy.Subscriber('%s/dist_to_obs' %turtle_name, Vector3, get_distobs)

    rate = rospy.Rate(100.0)
    flag_init = 0
    flag_rec = 0
    flag_enter_obs = 0
    min_dist = 100.0
    weight_exit = 1.0
    while not rospy.is_shutdown():
        try:
            (trans,rot) = listener.lookupTransform(ns_tf, ns+goal_name, rospy.Time(0))
            flag_rec = 1
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue
    	try:
            (trans2,rot2) = listener.lookupTransform('/world', ns+goal_name, rospy.Time(0))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue

        # print trans
        # print trans2


        vel_msg = Vector3()
    	dist_msg = Vector3()
        goal_msg = Vector3()
        magni = (trans[0]**2+trans[1]**2+trans[2]**2)**0.5
        # if(magni<2.0):
        #     vel_msg.x = KPx*trans[0] - KD*sp.linear.x
        #     vel_msg.y = KPy*trans[1] - KD*sp.linear.y
        #     vel_msg.z = KPz*trans[2] - KD*sp.linear.z
        # if(flag_obs.data == 0 and magni>2.0):
        #     vel_msg.x = 10*KPx*trans[0]/magni - KD*sp.linear.x
        #     vel_msg.y = 10*KPy*trans[1]/magni - KD*sp.linear.y
        #     vel_msg.z = 10*KPz*trans[2]/magni - KD*sp.linear.z
        #     # vel_msg.x = -KPx*(sp.linear.x - 10*trans[0]/magni)
        #     # vel_msg.y = -KPy*(sp.linear.y - 10*trans[1]/magni)
        #     # vel_msg.z = -KPz*(sp.linear.z - 10*trans[2]/magni)
        # elif(flag_obs.data == 0 and magni<=2.0):
        #     vel_msg.x = KPx*trans[0] - KD*sp.linear.x
        #     vel_msg.y = KPy*trans[1] - KD*sp.linear.y
        #     vel_msg.z = KPz*trans[2] - KD*sp.linear.z
        # else:
        #     vel_msg.x = 0.0
        #     vel_msg.y = 0.0
        #     vel_msg.z = 0.0

        if(flag_init == 0 and flag_rec == 1):
            initial_goal = (trans[0]**2+trans[1]**2+trans[2]**2)**0.5
            # print "init"
            # print initial_goal
            init_to_goal.data = initial_goal
            flag_init = 1

        if(goal_relax == 1):
            dist_obs = (dist_to_obs.x**2+dist_to_obs.y**2+dist_to_obs.z**2)**0.5
            for i in range(0,3):
                vect_goal[i,0] = trans[i]
            vect_obs[0,0] = dist_to_obs.x
            vect_obs[1,0] = dist_to_obs.y
            vect_obs[2,0] = dist_to_obs.z
            bound = 1.5
            bound_add = 2.5

            if(dist_obs<2*bound):
                if(dist_obs<bound_add):
                    if(flag_enter_obs == 0):
                        init_enter_obs = (trans[0]**2+trans[1]**2+trans[2]**2)**0.5
                        flag_enter_obs = 1
                    # print "init_enter_obs"
                    # print init_enter_obs
                    # print vect_goal.norm()
                else:
                    flag_enter_obs = 0
                cos_angle = (vect_goal.transpose()*vect_obs)/(vect_goal.norm()*vect_obs.norm())
                weight = 1-cos_angle[0,0]
                if(vect_goal.norm()>initial_goal):
                    new_weight = math.exp(-(vect_goal.norm()-initial_goal)/0.1)
                    # print new_weight
                else:
                    new_weight = 1

                if(flag_enter_obs == 1 and vect_goal.norm()>init_enter_obs):
                    weight_concave = math.exp(-(vect_goal.norm()-init_enter_obs)/1.0)
                else:
                    weight_concave = 1.0

                # Define the weight for exit collision_avoidance
                to_goal = (trans[0]**2+trans[1]**2+trans[2]**2)**0.5
                if(to_goal < min_dist):
                    min_dist = to_goal
                    weight_exit = 1.0
                else:
                    weight_exit = math.exp(-(to_goal-min_dist)/0.1)

                # KP = 0.0

            else:
                weight_exit = 1.0
                min_dist = 100.0
                flag_enter_obs = 0
                weight = 1
                new_weight = 1
                weight_concave = 1
            # KP = KP0/(1.0+math.exp(-(dist_obs-4.0*bound)/1.0))*weight
            # KP = KP0*(1.0-math.exp(-(dist_obs)/1.5))*weight*new_weight*weight_concave

            # Without weight_exit
            # weight_exit = 1.0
            KP = KP0*(1.0-math.exp(-(dist_obs)/1.5))*weight*new_weight*weight_exit
            KP_geom = KP_omega*(1.0-math.exp(-(dist_obs)/1.5))*weight*new_weight*weight_exit
            # KP_geom = KP_omega
            # print "new_weight"
            # print new_weight
            # print "weight_concave"
            # print weight_concave
            # print "weight_exit"
            # print weight_exit
            # print "weight"
            # print weight
            # print "KP"
            # print KP_geom
            # vel_msg.x = KP/KP0*(KPx*trans[0] - KD*sp.linear.x)
            # vel_msg.y = KP/KP0*(KPy*trans[1] - KD*sp.linear.y)
            # vel_msg.z = KP/KP0*(KPz*trans[2] - KD*sp.linear.z)


            # print omega_3
            # vel_attr = Matrix([[0.0],[0.0],[0.0]])
            # print Force_add
            # print vel_attr
        else:
            KP = KP0
            KP_geom = KP_omega

        if(speed_cont == 1):
            # Speed controller
            if(flag_rec == 1):
                rg = Matrix([[trans[0]], [trans[1]], [trans[2]]])
                agent_cur = Matrix([[sp.linear.x], [sp.linear.y], [sp.linear.z]])
                # Speed controller
                if(magni>magni_bound):
                    if(agent_cur.norm()==0):
                        s_ = rg/rg.norm()
                    else:
                        s_ = agent_cur/agent_cur.norm()
                    u = -KP/KP0*(agent_cur.norm() - speed_limit)
                    vel_attr = u*s_
                else:
                    # print "close"
                    vel_attr = KPx/KP0*rg - KD_speed*agent_cur
                # print "KP_speed"
                # print KP/KP0
            else:
                vel_attr = Matrix([[0.0],[0.0],[0.0]])


            # Try the geometric control for 3D
            if(magni>magni_bound):
                force_goal = Matrix([[vel_msg.x],[vel_msg.y],[vel_msg.z]])
                # agent_cur = Matrix([[sp.linear.x], [sp.linear.y], [sp.linear.z]])
                agent_cur = Matrix([[sp.linear.x], [sp.linear.y], [sp.linear.z]])
                # agent_cur = Matrix([[1.0], [0.0], [0.0]])
                speed_mag = agent_cur.norm()
                agent_target = Matrix([[trans[0]],[trans[1]],[trans[2]]])
                # agent_target = Matrix([[0.0],[1.0],[0.0]])
                if(agent_cur.norm()!=0):
                    agent_cur = agent_cur/(agent_cur.norm())
                    base_vector = Matrix([[1.0],[0.0],[0.0]])
                    omega_skew = skew_symm(skew_symm(base_vector)*agent_cur)
                    # print "omega_skew1"
                    # print omega_skew
                    cos_theta = agent_cur.dot(base_vector)
                    sin_theta = (skew_symm(base_vector)*agent_cur).norm()
                    if(cos_theta!=-1):
                        # R_1 = eye(3) + omega_skew + omega_skew*omega_skew*(1-cos_theta)/(sin_theta**2)
                        R_1 = eye(3) + omega_skew + omega_skew*omega_skew*1/(1+cos_theta)
                    else:
                        R_1 = Matrix([[-1.0, 0.0, 0.0], [0.0, -1.0, 0.0], [0.0, 0.0, 1.0]])
                    # print "tes"
                    # print agent_cur
                    # print cos_theta
                    # print sin_theta
                    # print R_1
                    if(agent_target.norm()!=0):
                        agent_target = agent_target/(agent_target.norm())
                    base_vector = Matrix([[1.0],[0.0],[0.0]])
                    omega_skew = skew_symm(skew_symm(base_vector)*agent_target)
                    # print "omega_skew2"
                    # print omega_skew
                    cos_theta = agent_target.dot(base_vector)
                    sin_theta = (skew_symm(base_vector)*agent_target).norm()
                    if(cos_theta!=-1):
                        # R_2 = eye(3) + omega_skew + omega_skew*omega_skew*(1-cos_theta)/(sin_theta**2)
                        R_2 = eye(3) + omega_skew + omega_skew*omega_skew*1/(1+cos_theta)
                    else:
                        R_2 = Matrix([[-1.0, 0.0, 0.0], [0.0, -1.0, 0.0], [0.0, 0.0, 1.0]])
                    # print "tes2"
                    # print agent_target
                    # print cos_theta
                    # print sin_theta
                    # print R_2
                else:
                    R_1 = eye(3)
                    R_2 = eye(3)
                # print "R_1"
                # print R_1
                # print "R_2"
                # print R_2
                # print "print"
                # This R_mat is in frame R_2, so the omega needs to be transformed back to world frame
                R_mat = R_2.transpose()*R_1
                # print "R_mat"
                # print R_mat
                cos_theta = (0.5*((R_mat.trace())-1))

                if(cos_theta>0.9999999):
                    cos_theta = 0.9999999
                elif(cos_theta<-0.9999999):
                    cos_theta = -0.9999999
                # print cos_theta
                theta = acos(cos_theta)
                # print "theta"
                # print theta
                if(sin(theta)!=0):
                    out = (theta/sin(theta))*(R_mat - R_mat.transpose())
                else:
                    out = (R_mat - R_mat.transpose())
                omega_so3 = -KP_geom*out
                # print "KP_geom"
                # print KP_geom
                # print omega_so3
                omega_3 = R_2*Matrix([[omega_so3[2,1]],[omega_so3[0,2]],[omega_so3[1,0]]])
                agent_cur = Matrix([[sp.linear.x], [sp.linear.y], [sp.linear.z]])
                # agent_cur = Matrix([[1.0], [0.0], [0.0]])
                Force_add = skew_symm(omega_3)*agent_cur
                # print Force_add
                # print omega_3
                # print "print"

            else:
                Force_add = Matrix([[0.0],[0.0],[0.0]])

            vel_msg.x = Force_add[0,0] + vel_attr[0,0]
            vel_msg.y = Force_add[1,0] + vel_attr[1,0]
            vel_msg.z = Force_add[2,0] + vel_attr[2,0]
        else:
            vel_msg.x = KP/KP0*(KPx*trans[0] - KD*sp.linear.x)
            vel_msg.y = KP/KP0*(KPy*trans[1] - KD*sp.linear.y)
            vel_msg.z = KP/KP0*(KPz*trans[2] - KD*sp.linear.z)

            # vel_msg.x = (KPx*trans[0] - KD*sp.linear.x)
            # vel_msg.y = (KPy*trans[1] - KD*sp.linear.y)
            # vel_msg.z = (KPz*trans[2] - KD*sp.linear.z)

        # energy = 0.5*(sp.linear.x**2+sp.linear.y**2+sp.linear.z**2) + 0.5*KPx*vect_goal.norm()**2
        # print energy

        # # Try zero speed_obs
        # vel_msg.x = 0.0
        # vel_msg.y = 0.0
        # vel_msg.z = 0.0



        # Another Magnetic-field-based go-to-goal
        # if(agent_cur.norm()!=0):
        #     agent_cur = agent_cur/(agent_cur.norm())
        #
        # Force_add = force_goal - force_goal.dot(agent_cur)*agent_cur
        # # print "Go_to_goal_gyro"
        # # print Force_add
        #
        # vel_msg.x = Force_add[0,0]
        # vel_msg.y = Force_add[1,0]
        # vel_msg.z = Force_add[2,0]

        # print "goal:"
        # print force_goal.dot(agent_cur)

        # # Magnetic-field-based go-to-goal
        # agent_cur = Matrix([[sp.linear.x], [sp.linear.y], [sp.linear.z]])
        # rg = Matrix([[trans[0]], [trans[1]], [trans[2]]])
        # la_cross = Matrix([[0, -agent_cur[2,0], agent_cur[1,0]], [agent_cur[2,0], 0, -agent_cur[0,0]], [-agent_cur[1,0], agent_cur[0,0], 0]])
        # rg_cross = Matrix([[0, -trans[2], trans[1]], [trans[2], 0, -trans[0]], [-trans[1], trans[0], 0]])
        # if(agent_cur.norm()!=0):
        #     goal_cur = (agent_cur/agent_cur.norm()) + (rg/rg.norm())
        #     goal_cur = goal_cur/goal_cur.norm()
        # else:
        #     goal_cur = rg/rg.norm()
        # goal_cross = Matrix([[0, -goal_cur[2], goal_cur[1]], [goal_cur[2], 0, -goal_cur[0]], [-goal_cur[1], goal_cur[0], 0]])
        # # Force = (agent_cur.dot(rg)/(rg.norm())**2)*la_cross*(rg_cross*agent_cur)
        # if(agent_cur.norm()!=0):
        #     # Force = (1/(rg.norm()))*la_cross*(rg_cross*agent_cur)
        #     Force = 0.1*la_cross*(goal_cross*agent_cur)
        # else:
        #     Force = zeros(3,1)
        #
        # print Force
        #


        # vel_msg.x = Force[0,0] + vel_attr[0,0]
        # vel_msg.y = Force[1,0] + vel_attr[1,0]
        # vel_msg.z = Force[2,0] + vel_attr[2,0]

        # vel_msg.x = KPx*trans[0] - KD*sp.linear.x
        # vel_msg.y = KPy*trans[1] - KD*sp.linear.y
        # vel_msg.z = KPz*trans[2] - KD*sp.linear.z
        dist_msg.x = trans[0]
        dist_msg.y = trans[1]
        dist_msg.z = trans[2]
        goal_msg.x = trans2[0]
        goal_msg.y = trans2[1]
        goal_msg.z = trans2[2]
        turtle_vel.publish(vel_msg)
        turtle_dist.publish(dist_msg)
        turtle_goal.publish(goal_msg)
        turtle_init.publish(init_to_goal)
        rate.sleep()
