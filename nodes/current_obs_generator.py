#!/usr/bin/env python  
import roslib
roslib.load_manifest('double_integrator')
import rospy
import math
import tf
from sympy import *
from geometry_msgs.msg import Vector3
from std_msgs.msg import String
from double_integrator.msg import vector3_array

def find_magnitude(input1, input2):
    magnitude = math.sqrt(input1 ** 2 + input2 ** 2)
    return magnitude

if __name__ == '__main__':
    rospy.init_node('current_obs')
    obstacle_number = rospy.get_param('/ObstacleNumber')
    obs_name = rospy.get_param('/ObstacleName')
    
    cur_sent = vector3_array()
    cur_3 = Vector3()
    listener = tf.TransformListener()

    current = rospy.Publisher('/current_obstacle', vector3_array, queue_size = 10)
    
    obstacle_index = String()
    obstacle_index2 = String()
    
    rate = rospy.Rate(10.0)
    
    while not rospy.is_shutdown():
      cur_sent.Vector3D = []
      for i in range(0, (obstacle_number)):
	obstacle_index = ''
	obstacle_index2 = ''
	obstacle_index = str(i+1)
	if(i!=obstacle_number):
	  obstacle_index2 = str(i+2)
	else:
	  obstacle_index2 = str(1)
	try:
            (trans,rot) = listener.lookupTransform(obs_name+obstacle_index2, obs_name+obstacle_index, rospy.Time(0))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue
	
	cur_vec = Matrix([[trans[0]], [trans[1]], [0]])
	cur_mag = find_magnitude(trans[0], trans[1])
	cur = cur_vec/(cur_mag)
	cur_3.x = cur[0,0]
	cur_3.y = cur[1,0]
	cur_3.z = cur[2,0]
	
	cur_sent.Vector3D.append(cur_3)
	
      current.publish(cur_sent)
      rate.sleep()