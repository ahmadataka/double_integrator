#!/usr/bin/env python
import roslib
roslib.load_manifest('double_integrator')
import rospy
import math
import tf
from sympy import *
from geometry_msgs.msg import Vector3
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from std_msgs.msg import Float64

class read_from_stdr(object):
  def __init__(self):

    rospy.init_node('read_robot')
    turtle_name = rospy.get_param('~robot')
    listener = tf.TransformListener()
    self.laser_data = LaserScan()
    obs_dist_sent = Vector3()
    speed_sent = Twist()
    heading_sent = Float64()
    heading_obs_sent = Float64()
    self.flag_sensor = 0
    self.speed = 0.0
    flag_listener = 0
    theta_rob = 0.0
    self.vel = [0.0, 0.0]
    turtle_vel = rospy.Publisher('/%s/twist' %turtle_name, Twist, queue_size = 10)
    heading = rospy.Publisher('/%s/heading' %turtle_name, Float64, queue_size = 10)
    sub_speed = rospy.Subscriber('/mobile_base/commands/velocity', Twist, self.get_speed)
    dist_obs = Vector3()

    rate = rospy.Rate(10.0)

    while not rospy.is_shutdown():
        try:
            # (trans,rot) = listener.lookupTransform('/world', turtle_name, rospy.Time(0))
            (trans,rot) = listener.lookupTransform('/odom', '/base_link', rospy.Time(0))
            flag_listener = 1
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue

        if(flag_listener == 1): #and rot[0]<=1.0 and rot[1]<=1.0 and rot[2]<=1.0 and rot[3]<=1.0):
            euler = tf.transformations.euler_from_quaternion(rot)
            theta_rob = euler[2]

        speed_sent.linear.x = self.speed*cos(theta_rob)
        speed_sent.linear.y = self.speed*sin(theta_rob)

        heading_sent.data = theta_rob
        heading.publish(heading_sent)
        turtle_vel.publish(speed_sent)
        rate.sleep()

  def get_speed(self,var):
    # self.flag_sensor = 1
    self.speed = var.linear.x
    self.vel[0] = var.linear.x
    self.vel[1] = var.linear.y

if __name__ == '__main__':
    try:
        read_from_stdr()
    except rospy.ROSInterruptException: pass
