#!/usr/bin/env python
import roslib
roslib.load_manifest('double_integrator')
import rospy

import tf
from geometry_msgs.msg import Pose

def handle_turtle_pose(msg, turtlename):
    ns = rospy.get_namespace()
    ns_tf = ns[1:-1]
    print ns_tf

    br = tf.TransformBroadcaster()
    br.sendTransform((msg.position.x, msg.position.y, msg.position.z),
                     tf.transformations.quaternion_from_euler(0, 0, 0),
                     rospy.Time.now(),
                     ns_tf,
                     "world")

if __name__ == '__main__':
    rospy.init_node('turtle_tf_broadcaster')
    turtlename = rospy.get_param('~turtle')
    rospy.Subscriber('%s/pose' % turtlename,
                     Pose,
                     handle_turtle_pose,
                     turtlename)
    rospy.spin()
