#!/usr/bin/env python
import roslib
roslib.load_manifest('double_integrator')
import rospy
import math
import tf
from sympy import *
from geometry_msgs.msg import Vector3
from geometry_msgs.msg import Twist
from std_msgs.msg import Float64

class write_to_stdr(object):
  def __init__(self):
    robot_vel = Twist()
    heading_d = Float64()
    omega_goal = Vector3()
    rospy.init_node('send_robot')
    turtle_name = rospy.get_param('~robot')
    robot_type = rospy.get_param('~robot_type')
    field = rospy.get_param('/field')
    self.vel = [0.0, 0.0]
    self.speed_save = [0.0, 0.0]
    self.vect_obs = Matrix([[0.0], [0.0]])
    self.vect_goal = Matrix([[0.0], [0.0]])
    self.angle = 0.0
    angle_d = 0.0
    KP0 = 1.0
    self.flag = 0
    self.omega = 0.0
    self.dist_obs = 100.0
    dist_to_obs = rospy.Publisher('/%s/cmd_vel' %turtle_name, Twist, queue_size = 10)
    angle_desired = rospy.Publisher('/%s/heading_goal' %turtle_name, Float64, queue_size = 10)
    omega_desired = rospy.Publisher('/%s/omega_goal' %turtle_name, Vector3, queue_size = 10)
    sub_laser = rospy.Subscriber('/%s/command_velocity' %turtle_name, Vector3, self.get_vel)
    sub_heading = rospy.Subscriber('/%s/heading' %turtle_name, Float64, self.get_angle)
    sub_omega = rospy.Subscriber('/%s/omega_avoidance' %turtle_name, Vector3, self.get_omega)
    sub_dist = rospy.Subscriber('/%s/dist_to_obs' %turtle_name, Vector3, self.get_dist)
    sub_dist_goal = rospy.Subscriber('/%s/dist_to_goal' %turtle_name, Vector3, self.get_dist_goal)
    rate = rospy.Rate(10.0)
    rospy.sleep(5.)
    while not rospy.is_shutdown():
        if(robot_type==1):
            if(self.flag == 1):
                if(field==2):
                    if(self.dist_obs<1.5):
                        cos_angle = (self.vect_goal.transpose()*self.vect_obs)/(self.vect_goal.norm()*self.vect_obs.norm())
                        weight = 1-cos_angle[0,0]
                        KP = KP0*(1-math.exp(-self.dist_obs/1.5))*weight
                        # KP = 0.0
                    else:
                        KP = KP0
                else:
                    KP = KP0
                robot_vel.linear.x = (self.vel[0]**2 + self.vel[1]**2)**0.5
                if(robot_vel.linear.x>0.5):
                    robot_vel.linear.x = 0.5
                # KP = 0.0
                # KP = KP0
                print KP
                # robot_vel.linear.x = 0.5
                angle_d = atan2(self.vel[1], self.vel[0])
                omega_goal.z = -KP*(self.angle - angle_d)
                robot_vel.angular.z = omega_goal.z + self.omega
                heading_d.data = angle_d
        else:
            if(self.flag == 1):
                dt = 0.1
                robot_vel.linear.x = self.speed_save[0] + self.vel[0]*dt
                robot_vel.linear.y = self.speed_save[1] + self.vel[1]*dt
                vel_mag = (robot_vel.linear.x**2+robot_vel.linear.y**2)**0.5
                print vel_mag
                if(vel_mag>1.0):
                    robot_vel.linear.x = robot_vel.linear.x/vel_mag
                    robot_vel.linear.y = robot_vel.linear.y/vel_mag
                self.speed_save[0] = robot_vel.linear.x
                self.speed_save[1] = robot_vel.linear.y

        dist_to_obs.publish(robot_vel)
        angle_desired.publish(heading_d)
        omega_desired.publish(omega_goal)
        rate.sleep()

  def get_vel(self,var):
    self.flag = 1
    self.vel[0] = var.x
    self.vel[1] = var.y

  def get_angle(self,var):
    self.angle = var.data

  def get_omega(self,var):
    self.omega = var.z

  def get_dist(self,vari):
    self.dist_obs = (vari.x**2+vari.y**2)**0.5
    self.vect_obs[0,0] = vari.x
    self.vect_obs[1,0] = vari.y

  def get_dist_goal(self,vari):
    self.vect_goal[0,0] = vari.x
    self.vect_goal[1,0] = vari.y

if __name__ == '__main__':
    try:
        write_to_stdr()
    except rospy.ROSInterruptException: pass
