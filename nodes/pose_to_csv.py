#!/usr/bin/env python
import ros_numpy
import roslib; roslib.load_manifest('inflatable_arm')
import rospy
# from std_msgs.msg import Float64MultiArray
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseArray
import numpy as np

class record_data(object):
  def __init__(self):
    rospy.init_node('pose_to_csv')
    self.agent_num = 2
    rec_tip1 = rospy.Subscriber('/agent1/agent/pose', Pose, self.p_read_callback1)
    rec_tip2 = rospy.Subscriber('/agent2/agent/pose', Pose, self.p_read_callback2)
    rec_tip3 = rospy.Subscriber('/agent3/agent/pose', Pose, self.p_read_callback3)
    rec_tip4 = rospy.Subscriber('/agent4/agent/pose', Pose, self.p_read_callback4)

    self.pose1 = Pose()
    self.pose2 = Pose()
    self.pose3 = Pose()
    self.pose4 = Pose()
    self.pose_save = PoseArray()
    self.index = 0
    print "starting"
    self.pose_save = []
    rate = rospy.Rate(100.0)
    while not rospy.is_shutdown():
        # Record CSV
        self.record_pose()
        rate.sleep()
    print "end"
    np.savetxt('agent2field2.csv', np.asarray(self.pose_save).reshape(self.index,6), delimiter=',')   # X is an array

  def record_pose(self):
      # self.pose_save = []
      self.index = self.index + 1
      # self.pose_save.append("x1")
      self.pose_save.append(self.pose1.position.x)
      # self.pose_save.append("y1")
      self.pose_save.append(self.pose1.position.y)
      # self.pose_save.append("z1")
      self.pose_save.append(self.pose1.position.z)
      # self.pose_save.append("x2")
      self.pose_save.append(self.pose2.position.x)
      # self.pose_save.append("y2")
      self.pose_save.append(self.pose2.position.y)
      # self.pose_save.append("z2")
      self.pose_save.append(self.pose2.position.z)

      # np.asarray(self.pose_save).reshape(self.index,6)

      # np.savetxt('agent2field2.csv', np.asarray(self.pose_save).reshape(1,6), delimiter=',')   # X is an array

  def p_read_callback1(self, msg):
    self.pose1.position.x = msg.position.x
    self.pose1.position.y = msg.position.y
    self.pose1.position.z = msg.position.z

  def p_read_callback2(self, msg):
    self.pose2.position.x = msg.position.x
    self.pose2.position.y = msg.position.y
    self.pose2.position.z = msg.position.z

  def p_read_callback3(self, msg):
    self.pose3.position.x = msg.position.x
    self.pose3.position.y = msg.position.y
    self.pose3.position.z = msg.position.z

  def p_read_callback4(self, msg):
    self.pose4.position.x = msg.position.x
    self.pose4.position.y = msg.position.y
    self.pose4.position.z = msg.position.z

if __name__ == '__main__':
    try:
        record_data()
    except rospy.ROSInterruptException: pass
