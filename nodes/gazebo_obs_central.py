#!/usr/bin/env python
import roslib
roslib.load_manifest('double_integrator')
import rospy
import math
from geometry_msgs.msg import Vector3
from gazebo_msgs.msg import LinkStates
import tf

obs_cen = Vector3()
robot_pose = Vector3()
obs_num = 0
def obs_get(variable):
  avg_x = 0
  avg_y = 0
  avg_z = 0
  for i in range(0,obs_num):
      avg_x = avg_x + variable.pose[1+i].position.x
      avg_y = avg_y + variable.pose[1+i].position.y
      avg_z = avg_z + variable.pose[1+i].position.z
  if(obs_num!=0):
      avg_x = avg_x /float(obs_num)
      avg_y = avg_y /float(obs_num)
      avg_z = avg_z /float(obs_num)
  obs_cen.x = avg_x - robot_pose.x
  obs_cen.y = avg_y - robot_pose.y
  obs_cen.z = avg_z - robot_pose.z

if __name__ == '__main__':

    rospy.init_node('get_obs')
    obs_num = rospy.get_param('/obs_num')
    br = tf.TransformBroadcaster()
    listener = tf.TransformListener()
    turtle_vel = rospy.Publisher('/obs_central', Vector3, queue_size = 10)
    sub_obs = rospy.Subscriber('/gazebo/link_states', LinkStates, obs_get)
    rate = rospy.Rate(10.0)
    while not rospy.is_shutdown():
        try:
            (trans,rot) = listener.lookupTransform('/odom', '/base_footprint', rospy.Time(0))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue
        br.sendTransform((obs_cen.x+robot_pose.x, obs_cen.y+robot_pose.y, obs_cen.z+robot_pose.z),
                           (0.0, 0.0, 0.0, 1.0),
                           rospy.Time.now(),
                           "obs_central",
                           "odom")
        robot_pose.x = trans[0]
        robot_pose.y = trans[1]
        robot_pose.z = trans[2]
        turtle_vel.publish(obs_cen)
        rate.sleep()
