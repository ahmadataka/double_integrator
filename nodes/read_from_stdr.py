#!/usr/bin/env python
import roslib
roslib.load_manifest('double_integrator')
import rospy
import math
import tf
from sympy import *
from geometry_msgs.msg import Vector3
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from std_msgs.msg import Float64

class read_from_stdr(object):
  def __init__(self):

    rospy.init_node('read_robot')
    turtle_name = rospy.get_param('~robot')
    robot_type = rospy.get_param('~robot_type')
    listener = tf.TransformListener()
    self.laser_data = LaserScan()
    obs_dist_sent = Vector3()
    speed_sent = Twist()
    heading_sent = Float64()
    heading_obs_sent = Float64()
    self.flag_sensor = 0
    self.speed = 0.0
    flag_listener = 0
    theta_rob = 0.0
    self.vel = [0.0, 0.0]
    turtle_vel = rospy.Publisher('/%s/twist' %turtle_name, Twist, queue_size = 10)
    dist_to_obs = rospy.Publisher('/%s/dist_to_obs' %turtle_name, Vector3, queue_size = 10)
    heading = rospy.Publisher('/%s/heading' %turtle_name, Float64, queue_size = 10)
    heading_obs = rospy.Publisher('/%s/heading_obs' %turtle_name, Float64, queue_size = 10)
    # dist_to_obs = rospy.Publisher('/robot0/dist_to_obs', Vector3, queue_size = 10)
    sub_laser = rospy.Subscriber('/%s/laser_0' %turtle_name, LaserScan, self.get_laser)
    # sub_laser = rospy.Subscriber('/robot0/laser_0', LaserScan, self.get_laser)
    sub_speed = rospy.Subscriber('/robot0/cmd_vel', Twist, self.get_speed)
    dist_obs = Vector3()

    rate = rospy.Rate(10.0)

    while not rospy.is_shutdown():
        try:
            # (trans,rot) = listener.lookupTransform('/world', turtle_name, rospy.Time(0))
            (trans,rot) = listener.lookupTransform('/world', '/robot0', rospy.Time(0))
            flag_listener = 1
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue

        if(flag_listener == 1): #and rot[0]<=1.0 and rot[1]<=1.0 and rot[2]<=1.0 and rot[3]<=1.0):
            euler = tf.transformations.euler_from_quaternion(rot)
            theta_rob = euler[2]
        minim = 100.0
        distance = minim
        index_min = 0
        if(self.flag_sensor==1):
            length = len(self.laser_data.ranges)
            for i in range(0,length):
                if(self.laser_data.ranges[i]<minim):
                    minim = self.laser_data.ranges[i]
                    index_min = i
                    angle = self.laser_data.angle_increment*index_min+self.laser_data.angle_min + 8.0*math.pi/180.0

        if(minim!=100):
            theta_obs = theta_rob + angle
            obs_dist_sent.x = minim*cos(theta_obs)
            obs_dist_sent.y = minim*sin(theta_obs)
            heading_obs_sent.data = angle*180.0/(math.pi)
            # print "theta_rob"
            # print theta_rob*180.0/3.14
            # print "angle"
            # print angle*180.0/3.14
            # print "theta_obs"
            # print theta_obs*180.0/3.14
            # print minim
            # print index_min
            # print angle

        # else:
        #     print "no obs"
        if(robot_type==1):
            speed_sent.linear.x = self.speed*cos(theta_rob)
            speed_sent.linear.y = self.speed*sin(theta_rob)
        else:
            speed_sent.linear.x = self.vel[0]
            speed_sent.linear.y = self.vel[1]
        heading_sent.data = theta_rob
        dist_to_obs.publish(obs_dist_sent)
        heading.publish(heading_sent)
        heading_obs.publish(heading_obs_sent)
        turtle_vel.publish(speed_sent)
        rate.sleep()

  def get_laser(self,var):
    self.flag_sensor = 1
    self.laser_data = var

  def get_speed(self,var):
    # self.flag_sensor = 1
    self.speed = var.linear.x
    self.vel[0] = var.linear.x
    self.vel[1] = var.linear.y

if __name__ == '__main__':
    try:
        read_from_stdr()
    except rospy.ROSInterruptException: pass
