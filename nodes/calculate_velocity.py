#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('double_integrator')
import math
import rospy
from geometry_msgs.msg import Vector3
from geometry_msgs.msg import Vector3Stamped
from geometry_msgs.msg import Twist

speed_goal = Vector3()
speed_obs = Vector3()
speed_ext = Vector3()
sp = Twist()
speed_total = Vector3Stamped()

def vel_goal(var):
  speed_goal.x = var.x
  speed_goal.y = var.y
  speed_goal.z = var.z

# def vel_obs(variable):
#   speed_obs.x = variable.x
#   speed_obs.y = variable.y
#   speed_obs.z = variable.z

def vel_obs_stamp(variable):
  speed_total.header.stamp.secs = variable.header.stamp.secs
  speed_total.header.stamp.nsecs = variable.header.stamp.nsecs

  speed_obs.x = variable.vector.x
  speed_obs.y = variable.vector.y
  speed_obs.z = variable.vector.z

def vel_ext(variable):
  speed_ext.x = variable.x
  speed_ext.y = variable.y
  speed_ext.z = variable.z

def speed_input(variable):
  sp.linear.x = variable.linear.x
  sp.linear.y = variable.linear.y
  sp.linear.z = variable.linear.z

def find_magnitude(input1, input2):
  magnitude = math.sqrt(input1 ** 2 + input2 ** 2)
  return magnitude

def calculate_velocity():
    rospy.init_node('calculate_velocity')
    turtle_name = rospy.get_param('~robot')
    #limit = rospy.get_param('/SpeedLimit')

    pub = rospy.Publisher('%s/command_velocity' %turtle_name, Vector3Stamped, queue_size = 10)
    rec_vel = rospy.Subscriber('%s/go_to_goal' %turtle_name, Vector3, vel_goal)
    # rec_vel2 = rospy.Subscriber('/%s/collision_avoidance' %turtle_name, Vector3, vel_obs)
    rec_vel2stamp = rospy.Subscriber('%s/collision_avoidance_stamped' %turtle_name, Vector3Stamped, vel_obs_stamp)
    sub_speed = rospy.Subscriber('%s/twist' %turtle_name, Twist, speed_input)
    rec_vel3 = rospy.Subscriber('force', Vector3, vel_ext)

    r = rospy.Rate(200) # 10hz
    while not rospy.is_shutdown():
        speed_total.vector.x = speed_goal.x + speed_obs.x + speed_ext.x
        speed_total.vector.y = speed_goal.y + speed_obs.y + speed_ext.y
        speed_total.vector.z = speed_goal.z + speed_obs.z + speed_ext.z

        pub.publish(speed_total)
        r.sleep()
        #rospy.spin()

if __name__ == '__main__':
    try:
        calculate_velocity()
    except rospy.ROSInterruptException: pass
