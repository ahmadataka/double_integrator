#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('double_integrator')
import rospy
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseArray
from geometry_msgs.msg import Vector3
from double_integrator.msg import Record

class record_data(object):
  def __init__(self):
    rospy.init_node('record_data')
    robot_type = rospy.get_param('~robot_type')
    if(robot_type==0):
        # self.obs_num = rospy.get_param('/ObstacleNumber')
        self.obs_num = 0
    else:
        self.obs_num = 0
    turtle_name = rospy.get_param('~robot')
    self.send = Record()

    send_pub = rospy.Publisher('/record_data', Record, queue_size = 10)
    rec_goal = rospy.Subscriber('/goal_pose', Vector3, self.goal_callback)
    rec_agent_pose = rospy.Subscriber('/%s/pose' %turtle_name, Pose, self.pose_callback)
    rec_obs_cur = rospy.Subscriber('/obstacle_current', Vector3, self.obs_cur_callback)
    rec_ag_cur = rospy.Subscriber('/agent_current', Vector3, self.ag_cur_callback)
    rec_obs1 = rospy.Subscriber('/obs1/pose', Pose, self.obspose_callback1)
    rec_obs2 = rospy.Subscriber('/obs2/pose', Pose, self.obspose_callback2)
    rec_obs3 = rospy.Subscriber('/obs3/pose', Pose, self.obspose_callback3)
    rec_obs4 = rospy.Subscriber('/obs4/pose', Pose, self.obspose_callback4)
    rec_obs5 = rospy.Subscriber('/obs5/pose', Pose, self.obspose_callback5)
    rec_obs6 = rospy.Subscriber('/obs6/pose', Pose, self.obspose_callback6)

    self.send = Record()
    self.obs_pose = Pose()
    self.obs1 = Pose()
    self.obs2 = Pose()
    self.obs3 = Pose()
    self.obs4 = Pose()
    self.obs5 = Pose()
    self.obs6 = Pose()
    self.flag1 = 0
    self.flag2 = 0
    self.flag3 = 0
    self.flag4 = 0
    self.flag5 = 0
    self.flag6 = 0
    frequency = 40.0
    r = rospy.Rate(frequency)

    while not rospy.is_shutdown():
    	self.transform_to_sent()
    	send_pub.publish(self.send)
        r.sleep()

  def goal_callback(self,var):
      self.send.goal.position.x = var.x
      self.send.goal.position.y = var.y
      self.send.goal.position.z = var.z

  def pose_callback(self, msg):
      self.send.robot_pose.position.x = msg.position.x
      self.send.robot_pose.position.y = msg.position.y
      self.send.robot_pose.position.z = msg.position.z
      self.send.robot_pose.orientation.w = msg.orientation.w
      self.send.robot_pose.orientation.x = msg.orientation.x
      self.send.robot_pose.orientation.y = msg.orientation.y
      self.send.robot_pose.orientation.z = msg.orientation.z

  def obs_cur_callback(self,var):
    self.send.obs_current.x = var.x
    self.send.obs_current.y = var.y

  def ag_cur_callback(self,var):
    self.send.robot_current.x = var.x
    self.send.robot_current.y = var.y

  def obspose_callback1(self, msg):
    self.obs1.position.x = msg.position.x
    self.obs1.position.y = msg.position.y
    self.obs1.position.z = msg.position.z
    self.flag1 = 1
  def obspose_callback2(self, msg):
    self.obs2.position.x = msg.position.x
    self.obs2.position.y = msg.position.y
    self.obs2.position.z = msg.position.z
    self.flag2 = 1
  def obspose_callback3(self, msg):
    self.obs3.position.x = msg.position.x
    self.obs3.position.y = msg.position.y
    self.obs3.position.z = msg.position.z
    self.flag3 = 1
  def obspose_callback4(self, msg):
    self.obs4.position.x = msg.position.x
    self.obs4.position.y = msg.position.y
    self.obs4.position.z = msg.position.z
    self.flag4 = 1
  def obspose_callback5(self, msg):
    self.obs5.position.x = msg.position.x
    self.obs5.position.y = msg.position.y
    self.obs5.position.z = msg.position.z
    self.flag5 = 1
  def obspose_callback6(self, msg):
    self.obs6.position.x = msg.position.x
    self.obs6.position.y = msg.position.y
    self.obs6.position.z = msg.position.z
    self.flag6 = 1

  def transform_to_sent(self):
    self.send.obstacles.poses = []
    if(self.flag1 == 1 and self.flag2 == 1 and self.flag3 == 1 and self.flag4 == 1 and self.flag5 == 1 and self.flag6 == 1):
        self.send.obstacles.poses.append(self.obs1)
        self.send.obstacles.poses.append(self.obs2)
        self.send.obstacles.poses.append(self.obs3)
        self.send.obstacles.poses.append(self.obs4)
        self.send.obstacles.poses.append(self.obs5)
        self.send.obstacles.poses.append(self.obs6)
        self.send.now = rospy.get_rostime()

if __name__ == '__main__':
    try:
        record_data()
    except rospy.ROSInterruptException: pass
