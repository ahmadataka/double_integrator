#!/usr/bin/env python
import roslib
roslib.load_manifest('double_integrator')
import rospy
import math
import tf
from sympy import *
from geometry_msgs.msg import Vector3
from geometry_msgs.msg import Twist
from std_msgs.msg import Float64
from std_msgs.msg import Int32

class write_to_stdr(object):
  def __init__(self):
    robot_vel = Twist()
    heading_d = Float64()
    omega_goal = Vector3()
    rospy.init_node('send_robot')
    turtle_name = rospy.get_param('~robot')
    goal_relax = rospy.get_param('/goal_rel')
    field = rospy.get_param('/field')
    self.vel = [0.0, 0.0]
    self.speed_save = [0.0, 0.0]
    self.vect_obs = Matrix([[0.0], [0.0]])
    self.vect_goal = Matrix([[0.0], [0.0]])
    self.angle = 0.0
    self.flag_range = 0
    angle_d = 0.0
    KP0 = 1.0
    self.flag = 0
    flag_init = 0
    self.flag_goal = 0
    self.omega = 0.0
    self.dist_obs = 100.0
    dist_to_obs = rospy.Publisher('/mobile_base/commands/velocity', Twist, queue_size = 10)
    angle_desired = rospy.Publisher('/%s/heading_goal' %turtle_name, Float64, queue_size = 10)
    omega_desired = rospy.Publisher('/%s/omega_goal' %turtle_name, Vector3, queue_size = 10)
    sub_laser = rospy.Subscriber('/%s/command_velocity' %turtle_name, Vector3, self.get_vel)
    sub_heading = rospy.Subscriber('/%s/heading' %turtle_name, Float64, self.get_angle)
    sub_omega = rospy.Subscriber('/%s/omega_avoidance' %turtle_name, Vector3, self.get_omega)
    sub_dist = rospy.Subscriber('/%s/dist_to_obs' %turtle_name, Vector3, self.get_dist)
    sub_dist_goal = rospy.Subscriber('/%s/dist_to_goal' %turtle_name, Vector3, self.get_dist_goal)
    sub_range = rospy.Subscriber('/out_of_range', Int32, self.get_range)
    rate = rospy.Rate(10.0)
    br = tf.TransformBroadcaster()
    rospy.sleep(5.)
    # print goal_relax
    while not rospy.is_shutdown():
        if(flag_init == 0 and self.flag_goal == 1):
            initial_goal = (self.vect_goal[0,0]**2+self.vect_goal[1,0]**2)**0.5
            # init_to_goal.data = initial_goal
            flag_init = 1
        if(self.flag == 1):
            if(goal_relax != 0):
                if(field==2):
                    if(self.dist_obs<3.0 and self.flag_range == 0):
                        if(goal_relax==2):
                            angle_obs = math.atan2(self.vect_obs[1,0],self.vect_obs[0,0])
                            angle_diff = (angle_obs - self.angle)
                            if(angle_diff>math.pi):
                                angle_diff = angle_diff - 2*math.pi
                            elif(angle_diff<-math.pi):
                                angle_diff = angle_diff + 2*math.pi
                            # print angle_diff
                            vect_head = Matrix([[math.cos(self.angle)],[math.sin(self.angle)]])
                            vect_obs_new = self.vect_obs - self.vect_obs.dot(vect_head)*vect_head

                            br.sendTransform((vect_obs_new[0,0], vect_obs_new[1,0], 0.0),
                                               (0.0, 0.0, 0.0, 1.0),
                                               rospy.Time.now(),
                                               "closest_obs_new",
                                               "/camera_depth_optical_frame")

                            if(angle_diff<-0.5 or angle_diff>0.5):
                                cos_angle = (self.vect_goal.transpose()*vect_obs_new)/(self.vect_goal.norm()*vect_obs_new.norm())
                            else:
                                cos_angle = (self.vect_goal.transpose()*self.vect_obs)/(self.vect_goal.norm()*self.vect_obs.norm())
                        elif(goal_relax==1):
                            cos_angle = (self.vect_goal.transpose()*self.vect_obs)/(self.vect_goal.norm()*self.vect_obs.norm())
                        weight = 1-cos_angle[0,0]
                        # print "weight"
                        # print weight

                        vect_head_3 = Matrix([[math.cos(self.angle)],[math.sin(self.angle)], [0.0]])
                        vect_goal_3 = Matrix([[self.vect_goal[0,0]], [self.vect_goal[1,0]], [0.0]])
                        vect_obs_3 = Matrix([[self.vect_obs[0,0]], [self.vect_obs[1,0]], [0.0]])

                        weight_dir = (self.skew_symm(vect_head_3)*vect_goal_3/vect_goal_3.norm()).dot(self.skew_symm(vect_head_3)*vect_obs_3/vect_obs_3.norm())

                        # theta_goal_head = (vect_head_3).dot(vect_goal_3)/vect_goal_3.norm()
                        # theta_obs_head = (vect_head_3).dot(vect_obs_3)/vect_obs_3.norm()
                        # weight_dir_new = sin(math.acos(theta_goal_head)/2.0)*sin(math.acos(theta_obs_head)/2.0)
                        weight_dir_new = 1/(1+math.exp(10*weight_dir))
                        # weight_dir_new = 1/(1+math.exp(10*weight_dir_new))
                        if(weight_dir>=0):
                            weight_new = 0
                        else:
                            weight_new = 1

                        # print "weight_new"
                        # print weight_new
                        print "weight_dir"
                        print weight_dir
                        print "weight_dir_new"
                        print weight_dir_new
                        weight = weight_dir_new
                        # if(self.vect_goal.norm()>initial_goal):
                        #     new_weight = math.exp(-(self.vect_goal.norm()-initial_goal)/0.1)
                        # else:
                        #     new_weight = 1

                    else:
                        weight = 1
                        # new_weight = 1
                    # print "weight"
                    # print weight
                    # KP = KP0*(1-math.exp(-self.dist_obs/1.5))*weight*new_weight
                    KP = KP0*(1-math.exp(-self.dist_obs/1.5))*weight
                    # print (1-math.exp(-self.dist_obs/1.5))
                    # print weight
                    # print new_weight
                    # print self.dist_obs
                    # print KP
                    # print "ok"

                else:
                    KP = KP0
            else:
                KP = KP0

            robot_vel.linear.x = (self.vel[0]**2 + self.vel[1]**2)**0.5
            if(robot_vel.linear.x>0.3):
                robot_vel.linear.x = 0.3

            angle_d = atan2(self.vel[1], self.vel[0])
            # omega_goal.z = -KP*(self.angle - angle_d)
            omega_goal.z = self.control_omega(self.angle,angle_d, KP)
            # omega_goal.z = 0.0
            # print "omega"
            # print omega_goal.z
            robot_vel.angular.z = omega_goal.z + self.omega
            # robot_vel.angular.z = self.omega
            heading_d.data = angle_d
        dist_to_obs.publish(robot_vel)
        # print robot_vel
        angle_desired.publish(heading_d)
        omega_desired.publish(omega_goal)
        rate.sleep()

  def get_vel(self,var):
    self.flag = 1
    self.vel[0] = var.x
    self.vel[1] = var.y

  def get_angle(self,var):
    self.angle = var.data

  def get_omega(self,var):
    self.omega = var.z

  def get_dist(self,vari):
    self.dist_obs = (vari.x**2+vari.y**2)**0.5
    self.vect_obs[0,0] = vari.x
    self.vect_obs[1,0] = vari.y

  def get_dist_goal(self,vari):
    self.vect_goal[0,0] = vari.x
    self.vect_goal[1,0] = vari.y
    self.flag_goal = 1

  def get_range(self, var):
    self.flag_range = var.data

  def skew_symm(self,mat_in):
      mat_out = Matrix([[0.0, 0.0, mat_in[1,0]], [0.0, 0.0, -mat_in[0,0]], [-mat_in[1,0], mat_in[0,0], 0.0]])
      return mat_out

  def control_omega(self, theta1, theta2, cons):
      R_1 = Matrix([[cos(theta1), -sin(theta1)],[sin(theta1), cos(theta1)]])
      R_2 = Matrix([[cos(theta2), -sin(theta2)],[sin(theta2), cos(theta2)]])
      R_mat = R_2.transpose()*R_1

      # Trace needs to be added by 1
      theta = acos(0.5*((R_mat.trace()+1)-1))
      out = (theta/sin(theta))*(R_mat - R_mat.transpose())
    #   y_so3 = 0.5*(R_mat - R_mat.transpose())
    #   y_3 = y_so3[1,0]
    #   print "sin"
    #   print y_3
    #   out = (asin(y_3)/y_3)*y_so3
      omega_so3 = -cons*out
      omega_r3 = float(omega_so3[1,0])
      return omega_r3

if __name__ == '__main__':
    try:
        write_to_stdr()
    except rospy.ROSInterruptException: pass
