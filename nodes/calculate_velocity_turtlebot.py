#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('double_integrator')
import math
import rospy
from geometry_msgs.msg import Vector3
from geometry_msgs.msg import Twist

speed_goal = Vector3()
speed_obs = Vector3()
speed_ext = Vector3()
sp = Twist()

def vel_goal(var):
  speed_goal.x = var.x
  speed_goal.y = var.y
  speed_goal.z = var.z

def vel_obs(variable):
  speed_obs.x = variable.x
  speed_obs.y = variable.y
  speed_obs.z = variable.z

def vel_ext(variable):
  speed_ext.x = variable.x
  speed_ext.y = variable.y
  speed_ext.z = variable.z

def speed_input(variable):
  sp.linear.x = variable.linear.x
  sp.linear.y = variable.linear.y
  sp.linear.z = variable.linear.z

def find_magnitude(input1, input2):
  magnitude = math.sqrt(input1 ** 2 + input2 ** 2)
  return magnitude

def calculate_velocity():
    rospy.init_node('calculate_velocity')
    turtle_name = rospy.get_param('~robot')
    #limit = rospy.get_param('/SpeedLimit')

    speed_total = Vector3()

    pub = rospy.Publisher('/%s/command_velocity' %turtle_name, Vector3, queue_size = 10)
    rec_vel = rospy.Subscriber('/%s/go_to_goal' %turtle_name, Vector3, vel_goal)
    rec_vel2 = rospy.Subscriber('/%s/collision_avoidance' %turtle_name, Vector3, vel_obs)
    sub_speed = rospy.Subscriber('/%s/twist' %turtle_name, Twist, speed_input)
    rec_vel3 = rospy.Subscriber('/force', Vector3, vel_ext)

    r = rospy.Rate(50) # 10hz
    while not rospy.is_shutdown():
        speed_total.x = speed_goal.x + speed_obs.x + speed_ext.x
        speed_total.y = speed_goal.y + speed_obs.y + speed_ext.y
        speed_total.z = speed_goal.z + speed_obs.z + speed_ext.z
        # print "tot:"
        # tes = sp.linear.x*speed_total.x + sp.linear.y*speed_total.y + sp.linear.z*speed_total.z
        # tes_goal = sp.linear.x*speed_goal.x + sp.linear.y*speed_goal.y + sp.linear.z*speed_goal.z
        tes_obs = sp.linear.x*speed_obs.x + sp.linear.y*speed_obs.y + sp.linear.z*speed_obs.z
        # print "tot:"
        # print tes
        # print "goal:"
        # print tes_goal
        print "obs:"
        print tes_obs
        print speed_obs
        print sp

        # if(tes>0):
        #     print tes
        #     print sp
        pub.publish(speed_total)
        r.sleep()
        #rospy.spin()

if __name__ == '__main__':
    try:
        calculate_velocity()
    except rospy.ROSInterruptException: pass
