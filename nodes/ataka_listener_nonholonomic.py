#!/usr/bin/env python
import roslib
roslib.load_manifest('double_integrator')
import rospy
import math
import tf
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Vector3

sp = Twist()
stiff = Vector3()
gravity = 0.2

def speed_input(variable):
  sp.linear.x = variable.linear.x
  sp.linear.y = variable.linear.y

def stiff_input(variable):
  stiff.x = variable.x
  stiff.y = variable.y

if __name__ == '__main__':

    rospy.init_node('ataka_listener')
    turtle_name = rospy.get_param('~robot')
    goal_name = rospy.get_param('~goal')
    robot_type = rospy.get_param('~robot_type')
    listener = tf.TransformListener()
    KPx = 0.1
    KPy = 0.1
    if(robot_type==1):
        KD = 0.0
    else:
        KD = 0.5

    turtle_vel = rospy.Publisher('/%s/go_to_goal' %turtle_name, Vector3, queue_size = 10)
    turtle_pose = rospy.Publisher('/%s/pose' %turtle_name, Pose, queue_size = 10)
    turtle_goal = rospy.Publisher('/goal_pose', Vector3, queue_size = 10)
    turtle_dist = rospy.Publisher('/%s/dist_to_goal' %turtle_name, Vector3, queue_size = 10)
    sub_speed = rospy.Subscriber('/%s/twist' %turtle_name, Twist, speed_input)
    sub_stiff = rospy.Subscriber('/stiffness', Vector3, stiff_input)

    rate = rospy.Rate(10.0)
    while not rospy.is_shutdown():
        try:
            (trans,rot) = listener.lookupTransform('/world', turtle_name, rospy.Time(0))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue
    	try:
            (trans2,rot2) = listener.lookupTransform('/world', goal_name, rospy.Time(0))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue
    	vel_msg = Vector3()
    	dist_msg = Vector3()
        goal_msg = Vector3()
        robot_msg = Pose()
    	vel_msg.x = -KPx*(trans[0]-trans2[0]) - KD*sp.linear.x
        vel_msg.y = -KPy*(trans[1]-trans2[1]) - KD*sp.linear.y
        dist_msg.x = trans2[0]-trans[0]
        dist_msg.y = trans2[1]-trans[1]
        goal_msg.x = trans2[0]
        goal_msg.y = trans2[1]
        robot_msg.position.x = trans[0]
        robot_msg.position.y = trans[1]
        robot_msg.orientation.z = rot[2]
        robot_msg.orientation.w = rot[3]
        turtle_vel.publish(vel_msg)
        turtle_dist.publish(dist_msg)
        turtle_goal.publish(goal_msg)
        turtle_pose.publish(robot_msg)
        rate.sleep()
