#!/usr/bin/env python
import roslib
roslib.load_manifest('double_integrator')
import rospy
import math
import tf
from geometry_msgs.msg import Pose

if __name__ == '__main__':
    rospy.init_node('obstacle')

    pub_pose = rospy.Publisher('/obs1/pose', Pose, queue_size = 10)
    pub_pose2 = rospy.Publisher('/obs2/pose', Pose, queue_size = 10)
    pub_pose3 = rospy.Publisher('/obs3/pose', Pose, queue_size = 10)
    pub_pose4 = rospy.Publisher('/obs4/pose', Pose, queue_size = 10)
    pub_pose5 = rospy.Publisher('/obs5/pose', Pose, queue_size = 10)
    pub_pose6 = rospy.Publisher('/obs6/pose', Pose, queue_size = 10)
    pub_pose7 = rospy.Publisher('/obs7/pose', Pose, queue_size = 10)
    pub_pose8 = rospy.Publisher('/obs8/pose', Pose, queue_size = 10)
    pub_pose9 = rospy.Publisher('/obs9/pose', Pose, queue_size = 10)
    pub_pose10 = rospy.Publisher('/obs10/pose', Pose, queue_size = 10)

    freq = 10.0
    system_pose = Pose()
    system_pose2 = Pose()
    system_pose3 = Pose()
    system_pose4 = Pose()
    system_pose5 = Pose()
    system_pose6 = Pose()
    system_pose7 = Pose()
    system_pose8 = Pose()
    system_pose9 = Pose()
    system_pose10 = Pose()

    rate = rospy.Rate(freq)
    delta = 0.3
    while not rospy.is_shutdown():
    #   system_pose.position.x = 5.3+2*delta
    #   system_pose.position.y = 5.5-2*delta
    #   system_pose3.position.y = 5.5+3*delta
    #   system_pose3.position.x = 5.3-9*delta
    #   system_pose4.position.x = 5.3-9*delta
    #   system_pose4.position.y = 5.5+10*delta
    #   system_pose5.position.x = 5.3-5*delta
    #   system_pose5.position.y = 5.5+15*delta
    #   system_pose6.position.x = 6
    #   system_pose6.position.y = 5.5+23*delta
      system_pose.position.x = 5.5+3*delta
      system_pose.position.y = 5.5-3*delta
      system_pose2.position.x = 5.5-3*delta+0.1
      system_pose2.position.y = 5.5+3*delta+0.1
      system_pose3.position.x = 5.3-9*delta
      system_pose3.position.y = 5.5+10*delta
      system_pose4.position.x = 5.3-5*delta
      system_pose4.position.y = 5.5+15*delta
      system_pose5.position.x = 6
      system_pose5.position.y = 5.5+23*delta
      system_pose6.position.x = 9+3*delta
      system_pose6.position.y = 9-3*delta
      #system_pose7.position.x = 5.3+4*delta
      #system_pose7.position.y = 5.5-4*delta
      #system_pose8.position.x = 5.3+5*delta
      #system_pose8.position.y = 5.5-5*delta
      #system_pose9.position.x = 5.3+6*delta
      #system_pose9.position.y = 5.5-6*delta
      #system_pose10.position.x = 5.3+7*delta
      #system_pose10.position.y = 5.5-7*delta

      pub_pose.publish(system_pose)
      pub_pose2.publish(system_pose2)
      pub_pose3.publish(system_pose3)
      pub_pose4.publish(system_pose4)
      pub_pose5.publish(system_pose5)
      pub_pose6.publish(system_pose6)
      #pub_pose7.publish(system_pose7)
      #pub_pose8.publish(system_pose8)
      #pub_pose9.publish(system_pose9)
      #pub_pose10.publish(system_pose10)
      rate.sleep()
