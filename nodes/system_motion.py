#!/usr/bin/env python
import roslib
roslib.load_manifest('double_integrator')
import rospy
import math
import tf
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Vector3
from geometry_msgs.msg import Vector3Stamped
from std_msgs.msg import Int32
from sensor_msgs.msg import Joy

class system_motion(object):
  def __init__(self):
      self.flag_obs = Int32()
      self.acc = Vector3()
      self.acc_read = Vector3()
      self.speed_goal = Vector3()
      self.speed_obs = Vector3()
      self.speed_ext = Vector3()
      self.time_sec_rec = Int32()
      self.time_nsec_rec = Int32()
      self.time_sec_sent = Int32()
      self.time_nsec_sent = Int32()
      rospy.init_node('system_motion')
      turtle_name = rospy.get_param('~robot')
      self.dim = rospy.get_param('~dim')
      x_start = rospy.get_param('~x')
      y_start = rospy.get_param('~y')
      z_start = rospy.get_param('~z')

      pub_pose = rospy.Publisher('agent/pose', Pose, queue_size = 10)
      pub_twist = rospy.Publisher('agent/twist', Twist, queue_size = 10)
      rec_vel = rospy.Subscriber('%s/go_to_goal' %turtle_name, Vector3, self.vel_goal)
      rec_joy = rospy.Subscriber('/joy', Joy, self.ps3_callback)
      # rec_vel2 = rospy.Subscriber('/%s/collision_avoidance' %turtle_name, Vector3, vel_obs)
    #   rec_vel2stamp = rospy.Subscriber('/%s/collision_avoidance_stamped' %turtle_name, Vector3Stamped, self.vel_obs_stamp)
      sub_input = rospy.Subscriber('agent/command_velocity', Vector3Stamped, self.acc_input)
      rec_vel3 = rospy.Subscriber('force', Vector3, self.vel_ext)
      self.flag_obs.data = 1
      freq = 50.0
      dt = 1/freq
      self.system_twist = Twist()
      system_pose = Pose()
      self.vel_save = Twist()
      pose_save = Pose()

      rate = rospy.Rate(freq)
    #   self.vel_save.linear.x = -1.0
    #   self.vel_save.linear.y = 0.0
    #   self.vel_save.linear.z = 0.0
      self.vel_save.linear.x = 0.0
      self.vel_save.linear.y = 0.0
      self.vel_save.linear.z = 0.0


    #   self.vel_save.linear.x = 0.509159905031
    #   self.vel_save.linear.y = 0.835045654555
    #   self.vel_save.linear.z = -0.250435209654
    #   pose_save.position.x = 16.1096141978
    #   pose_save.position.y = 10.0941247831
    #   pose_save.position.z = -2.74442681037

    #   self.vel_save.linear.x = 2.5
    #   self.vel_save.linear.y = 2.5
    #   self.vel_save.linear.z = 0.0
      pose_save.position.x = x_start
      pose_save.position.y = y_start
      pose_save.position.z = z_start

      system_pose.position.x = pose_save.position.x
      system_pose.position.y = pose_save.position.y
      system_pose.position.z = pose_save.position.z

      self.system_twist.linear.x = self.vel_save.linear.x
      self.system_twist.linear.y = self.vel_save.linear.y
      self.system_twist.linear.z = self.vel_save.linear.z

      self.move_flag = 0
      mag = (self.vel_save.linear.x**2+self.vel_save.linear.y**2+self.vel_save.linear.z**2)**0.5
    #   rospy.sleep(5.)
      while not rospy.is_shutdown():
        if(self.move_flag == 1):
            # self.acc.x = self.speed_goal.x + self.speed_obs.x + self.speed_ext.x
            # self.acc.y = self.speed_goal.y + self.speed_obs.y + self.speed_ext.y
            # self.acc.z = self.speed_goal.z + self.speed_obs.z + self.speed_ext.z
            self.acc.x = self.acc_read.x
            self.acc.y = self.acc_read.y
            if(self.dim == 3):
                self.acc.z = self.acc_read.z
            mag_now = (self.system_twist.linear.x**2+self.system_twist.linear.y**2+self.system_twist.linear.z**2)**0.5

            system_pose.position.x = pose_save.position.x + self.vel_save.linear.x*dt
            system_pose.position.y = pose_save.position.y + self.vel_save.linear.y*dt
            if(self.dim==3):
                system_pose.position.z = pose_save.position.z + self.vel_save.linear.z*dt

            if(self.flag_obs.data == 1):
                #   test_dot = self.speed_obs.x*self.vel_save.linear.x + self.speed_obs.y*self.vel_save.linear.y + self.speed_obs.z*self.vel_save.linear.z
                #   test_dot = self.speed_obs.x*self.system_twist.linear.x + self.speed_obs.y*self.system_twist.linear.y + self.speed_obs.z*self.system_twist.linear.z
                  if(self.dim==3):
                      test_dot = self.acc_read.x*self.system_twist.linear.x + self.acc_read.y*self.system_twist.linear.y + self.acc_read.z*self.system_twist.linear.z
                  else:
                      test_dot = self.acc_read.x*self.system_twist.linear.x + self.acc_read.y*self.system_twist.linear.y
                #   print test_dot
                  self.time_sec_sent.data = rospy.get_rostime().secs
                  self.time_nsec_sent.data = rospy.get_rostime().nsecs

                  if(test_dot > 0.001):
                      print "system_motion_in:"
                      print self.acc_read
                    #   print self.speed_obs
                      print self.vel_save
                      print self.system_twist
                      print test_dot
                      print self.time_sec_sent.data
                      print self.time_nsec_sent.data

                      print self.time_sec_rec.data
                      print self.time_nsec_rec.data

                  self.system_twist.linear.x = self.vel_save.linear.x + self.acc.x*dt
                  self.system_twist.linear.y = self.vel_save.linear.y + self.acc.y*dt
                  if(self.dim==3):
                      self.system_twist.linear.z = self.vel_save.linear.z + self.acc.z*dt

                  self.vel_save.linear.x = self.system_twist.linear.x
                  self.vel_save.linear.y = self.system_twist.linear.y
                  if(self.dim==3):
                      self.vel_save.linear.z = self.system_twist.linear.z

                  self.flag_obs.data = 0

            pose_save.position.x = system_pose.position.x
            pose_save.position.y = system_pose.position.y
            if(self.dim==3):
                pose_save.position.z = system_pose.position.z
            # pub_pose.publish(pose_save)
            # pub_twist.publish(self.vel_save)
        pub_pose.publish(system_pose)
        pub_twist.publish(self.system_twist)

        # print "mag_now"
        # print mag_now

        rate.sleep()


  def vel_goal(self,var):
    self.speed_goal.x = var.x
    self.speed_goal.y = var.y
    if(self.dim==3):
        self.speed_goal.z = var.z

  def ps3_callback(self,msg):
    # Keyboard 'x'
    if(msg.axes[2]==1):
        self.move_flag = 1

# def vel_obs(variable):
#   # self.flag_obs.data = 1
#   self.speed_obs.x = variable.x
#   self.speed_obs.y = variable.y
#   self.speed_obs.z = variable.z

  # def vel_obs_stamp(self,variable):
  #   self.time_sec_rec.data = variable.header.stamp.secs
  #   self.time_nsec_rec.data = variable.header.stamp.nsecs
  #
  #   # limit_dura = 25000000
  #   limit_dura = 20000000
  #   if(self.time_sec_rec.data > self.time_sec_sent.data):
  #       if(self.time_nsec_rec.data + 1000000000 -self.time_nsec_sent.data > limit_dura):
  #         self.speed_obs.x = variable.vector.x
  #         self.speed_obs.y = variable.vector.y
  #         self.speed_obs.z = variable.vector.z
  #         self.flag_obs.data = 1
  #
  #   elif(self.time_sec_rec.data == self.time_sec_sent.data):
  #       if(self.time_nsec_rec.data - self.time_nsec_sent.data > limit_dura):
  #         self.speed_obs.x = variable.vector.x
  #         self.speed_obs.y = variable.vector.y
  #         self.speed_obs.z = variable.vector.z
  #         self.flag_obs.data = 1

  def acc_input(self,variable):
      self.time_sec_rec.data = variable.header.stamp.secs
      self.time_nsec_rec.data = variable.header.stamp.nsecs

      limit_dura = 25000000
    #   limit_dura = 20000000
      if(self.time_sec_rec.data > self.time_sec_sent.data):
          if(self.time_nsec_rec.data + 1000000000 -self.time_nsec_sent.data > limit_dura):
            self.acc_read.x = variable.vector.x
            self.acc_read.y = variable.vector.y
            if(self.dim==3):
                self.acc_read.z = variable.vector.z
            self.flag_obs.data = 1

      elif(self.time_sec_rec.data == self.time_sec_sent.data):
          if(self.time_nsec_rec.data - self.time_nsec_sent.data > limit_dura):
            self.acc_read.x = variable.vector.x
            self.acc_read.y = variable.vector.y
            if(self.dim==3):
                self.acc_read.z = variable.vector.z
            self.flag_obs.data = 1

  def vel_ext(self,variable):
    self.speed_ext.x = variable.x
    self.speed_ext.y = variable.y
    if(self.dim==3):
        self.speed_ext.z = variable.z

if __name__ == '__main__':
    try:
        system_motion()
    except rospy.ROSInterruptException: pass
